// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// Copyright (C) - INRIA - Alessandro Monti, Claire Médigue, Michel Sorine
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [signals_energy,signals_dispersion,inout_gains,inout_coherences,report]=np_mc_nss_analysis(signals,frequency_bands,options)
//Non parametric multi channel non stationnary signal analysis signal-based
// non-stationary system analysis using short-time Fourier transform (STFT)
// by which the time record is multiplied by a sliding window and the FT of
// the consecutive windowed segments are computed resulting in a series of
// local spectra.
//INPUTS
//signals : a N by Nt array with as many rows as signals.  The first
//          row must be the output signal  and the next ones the
//          inputs.
//frequency_bands: a m by 2 array, each row [fmin fmax] specifies the
//          frequency range to be studied
//options : a struct with fields:
  
//     sectionlength: an integer, the signal section length to be used for
//           power and cross spectrum computation.
  
//     sectionstep: an integer, the step to be applied for one signal
//           section to the next one. sectionstep must be less or equal to
//           sectionlength. Overlap is sectionlength-sectionstep
  
//     smoothwindowlength: an integer, the length of the smoothing window
//           (hanning)
  
//     minimalcoherence: a positive number in ]0 1]
  
//signals_energy: a 2D array with N rows. Each row gives
//          the corresponding signal energy
//signals_dispersion: a tlist with fields:
//     fmin: a 2D array with N rows. Each row gives the frequency around
//          which the dispersion is minimal for the corresponding signal.
//     pmin: a 2D array with N rows. Each row gives the value of the SPSD at
//          the frequency of minimal dispersion for the corresponding signal. 
//     dmin: a 2D array with N rows. Each row gives the value defined within
//          the interval [0;1] that describes the dispersion of the
//          spectra. Dispersion value is zero when the signal is merely a
//          sinusoide, only one components, whereas is 1 if the spectra is
//          completely spread, so there isn't a principal component.
//inout_gains : a 2D array with N-1 rows. The input/output transfer
//          function gain
//inout_coherences : a 2D array with N-1 rows. The input/output coherence


//References: Rapport de recherche INRIA RR-4427:"Short term control of
//      the cardiovascular system: modelling and signal analysis"
//      Alessandro Monti, Claire Médigue, Michel Sorine,2002 pp 50-
  report=[];
  options= check_npmcnssa_options(options,"np_mc_nss_analysis")
  section_length=options.sectionlength;
  section_step=options.sectionstep;

  if section_step==[] then section_step=section_length/2,end

  //frequency discretization
  frq=(0:(section_length-1))/section_length;
  ind=zeros(frequency_bands);
  for l=1:size(frequency_bands,1)
    frequency_band=frequency_bands(l,:);
    if %t then
      ind(l,1)=find(frequency_band(1)>=frq(1:$-1)&frequency_band(1)<frq(2:$));
      ind(l,2)=find(frequency_band(2)>frq(1:$-1)&frequency_band(2)<=frq(2:$));
    else
      [m,k]=min(abs(frq-frequency_band(1)))
      if frq(k)>frequency_band(1) then k=k-1,end
      ind(l,1)=k;
    
      [m,k]=min(abs(frq-frequency_band(2)))
      if frq(k)<frequency_band(2) then k=k+1,end
      ind(l,2)=k;
    end
  end
  report=find(min(ind(:,2)-ind(:,1))<3)
  
  [Nsignals,Npoints]=size(signals);
  //smooth_window_length must be  less  than N and odd (to be ckecked) 
  //The section window is used to focus on the section middle to reduce
  //the non stationarity effect during the associated interval of time?
   section_window=window("hm",section_length);

   smoothing_window=window("hn",options.smoothwindowlength);
   smoothing_window=smoothing_window/sum(smoothing_window);
   
   //loop on batchs
   nbatch=floor((Npoints-section_length)/section_step)
   nfb=size(frequency_bands,1)


   //allocate arrays 
   //signal properties
   signals_dispersion_fmin=zeros(Nsignals,nbatch,nfb);
   signals_dispersion_pmin=zeros(Nsignals,nbatch,nfb);
   signals_dispersion_dmin=zeros(Nsignals,nbatch,nfb);
   signals_energy=zeros(Nsignals,nbatch,nfb);
   //inputs->output cross properties
   inout_gains = zeros(Nsignals-1,nbatch,nfb);
   inout_coherences= zeros(Nsignals-1,nbatch,nfb);
   i=0;
   for k=1:section_step:Npoints-section_length
     i=i+1;
     sect=k:k+section_length-1
     for ksig=1:Nsignals
       //Complex spectrum 
       sig_fft=fft(signals(ksig,sect).*section_window)
       //smoothed power spectral density (periodogram)
       spsd=Smooth(abs(sig_fft).^2/section_length,smoothing_window)
       if ksig==1 then 
         // preserve output signal data needed for cross properties
         sig_fft_out=sig_fft
         spsd_out=spsd
       else
         //cross properties
         scsd=Smooth(sig_fft_out.*conj(sig_fft)/section_length,smoothing_window)
         //input->output gain
         gain=abs(scsd)./spsd
         //input->output coherence
         coherence=abs(scsd).^2 ./ (spsd_out.*spsd)
       end
       for l=1:size(frequency_bands,1)
         //frequency points associated with current frequency band
         indexrange=ind(l,1):ind(l,2);
         //signals dispersion 
         [imin,pmin,dmin]=DispersionBand(spsd(indexrange));
//         if k>5 then disp(l);scf(10);clf;plot(indexrange,spsd(indexrange),indexrange(imin),spsd(indexrange(imin)),'or'),halt;end
        // mprintf("%.2g\n",frq(indexrange(imin))*freq_sampling);
         signals_dispersion_fmin(ksig,i,l)=frq(indexrange(imin));
         signals_dispersion_pmin(ksig,i,l)=pmin;
         signals_dispersion_dmin(ksig,i,l)=dmin;
         //signals energy (sum of the periodogram part within frequency band)
         signals_energy(ksig,i,l)=sum(spsd(indexrange))
         if ksig>1 then
           inout_gains(ksig-1,i,l)=mean(gain(indexrange));
           klast=find(coherence(ind(l,1):ind(l,2))<options.minimalcoherence,1);
           if klast==[] then klast=ind(l,2),else klast=ind(l,1)-1+klast;end
           inout_coherences(ksig-1,i,l)=mean(coherence(ind(l,1):klast));
           if inout_coherences(ksig-1,i,l)<options.minimalcoherence then
             inout_gains(ksig-1,i,l)=%nan
           end
         end
       end
      
     end
   end
   signals_dispersion=tlist(["sigdisp","fmin","pmin","dmin"],...
                            signals_dispersion_fmin,...
                            signals_dispersion_pmin,...
                            signals_dispersion_dmin)
    
endfunction
//local functions

function xs=Smooth(x,win)
  n=size(win,'*');
  half=(n-1)/2;
  xs=convol(win,x)
  xs=xs(1+half:$-half)
endfunction


function [fmin,pmin,dmin]=DispersionBand(psd)
  //psd :power spectral density array
    
//fmin: frequency index that gives the minimum dispersion
//pmin: associated power
//dmin: associated dispersion
  n=size(psd,'*');
  if n==0 then
    fmin=0
    pmin=0
    dmin=-1
  elseif n==1 then
    pmin=psd
    dmin=1
    fmin=1
  else
    psd=matrix(psd,1,-1);
    dmin=%inf;
    //find point that realizes the minimal dispersion index
    for i=find(psd>0)
      //compute the dispersion value for frequency point i
      f=((1:n)-i).^2;
      dispersion=sum(f.*psd)./sum(f*psd(i))
      if dispersion<dmin//|(dmin==-1&dispersion<>dmin) then
        fmin=i
        pmin=psd(i)
        dmin=dispersion
      end
    end
    if dmin==%inf then 
      pmin=0
      fmin=1
      dmin=1,
    end
  end
endfunction


