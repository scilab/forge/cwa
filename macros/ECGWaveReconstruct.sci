//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [T,S]=ECGWaveReconstruct(fs,Ts,G)
  if type(fs)<>1|~isreal(fs)|size(fs,"*")<>1 then
    error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),...
                   "ECGWaveReconstruct",1))
  end
  if fs<=0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),...
                   "ECGWaveReconstruct",1))
  end
  if type(Ts)<>1|~isreal(Ts) then
    error(msprintf(_("%s: Wrong type for argument %d: Real array expected.\n"),...
                   "ECGWaveReconstruct",2))
  end
  if or(diff(Ts)<=0) then
    error(msprintf(_("%s: Wrong values for input argument #%d: Elements must be in increasing order.\n"),...
                   "ECGWaveReconstruct",2))
  end
  if ndims(G)<>3 then
     error(msprintf(_("%s: Wrong size for input argument #%d: A real 3-dimension hypermatrix ""expected.\n"),...
                   "ECGWaveReconstruct",3))
  end
  N=size(G,3); //number of beats
  S=[];T=[];B=[];
  for k=1:N
    n=round((Ts(k+1)-Ts(k)-1/fs)*fs)
    t=(0:n)/n;
    s=ECGWaveGen(t,G(:,:,k));
    if k==1 then 
      S=[S s]
      T=[T round(Ts(k)*fs)/fs+(0:n)/fs]
    else
      tend=round(T($)*fs);
      tstart=round(Ts(k)*fs)
      if tstart<=tend then
        nt=tend-tstart+1
        T=[T T($)+(nt:n)/fs]
        S=[S s(1+nt:$)]
      elseif tstart>tend+1 then
        nt=tstart-tend-1
        T=[T T($)+(1:n+nt+1)/fs]
        S=[S s(ones(1,nt)),s]
      else
        S=[S s]
        T=[T T($)+(1:n+1)/fs]
      end
    end
  end
endfunction
