//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [g,gl] = grp2idx(v)
//Create index vector from a grouping variable.
// g contains the unique values of . If v is a vector of floating point numbers, the %nan values are omitted
// gl is a vector of indexes relative to g such that g(gl)=v
  sz=size(v)
  [gl,k]=gsort(v(:),'g','i');
  d=gl(2:$)==gl(1:$-1);
  gl(d)=[];
  g=zeros(k);
  g(k)=cumsum([1;bool2s(~d)])
  if type(v)==1 then
    ii=find(isnan(gl))
    gl(ii)=[];
    g(g>size(gl,'*'))=%nan
  end
  g=matrix(g,sz)
endfunction
