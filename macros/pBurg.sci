function [PSD,F]=pBurg(y,AR_order,varargin);
  if argn(2)<2 then 
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),"pBurg",2,5))
  end
  
  if type(y)<>1|and(size(y)>1)|~isreal(y) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A  real vector expected.\n"),"pBurg",1))
  end
  if type(AR_order)<>1|size(AR_order,'*')<>1|~isreal(AR_order)|AR_order<=0|int(AR_order)<>AR_order then
    error(msprintf(_("%s: Wrong type for input argument #%d: A positive integer expected.\n"),"pBurg",2))
  end
  
  [F,fs,opt,scal]=pBurg_options(varargin)
  [a,v]=arBurg(y,AR_order);
  h=horner(%z^AR_order/poly(a($:-1:1),'z','c'),exp((2*%i*%pi/fs)*F ))
  PSD= (v/(fs*scal))*abs(h.^2);
  F=F(:);PSD=PSD(:);
  if opt=="onesided" then
    n=ceil((size(F,'*')+1)/2);
    F=F(1:n);
    PSD=2*PSD(1:n);
  end
endfunction

function [F,fs,opt,scal]=pBurg_options(optlist)
  //nf,fs,opt
  withF=%f

  nopt=size(optlist)
  
  if nopt<2|optlist(2)==[] then
    fs=1;scal=2*%pi
  else
    fs=optlist(2);scal=1
    if type(fs)<>1|size(fs,'*')<>1|fs<=0 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A positive real expected.\n"),"pBurg",4))
    end
  end
  
  nf=256
  F=(0:(nf-1))*(fs/nf);
  if nopt>=1&optlist(1)<>[]  then
    nf=optlist(1)
    if type(nf)<>1|or(nf<0) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A positive real array expected.\n"),"pBurg",3))
    end
    if size(nf,'*')==1 then
      F=(0:(nf-1))*(fs/nf)
    else
      F=nf
      nf=size(F,'*')
      withF=%t
    end
  end
  
  if nopt<3 then 
    if withF then opt="twosided"; else opt="onesided";end
  else
    opt=optlist(3)
    if type(opt)<>10|size(opt,'*')<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A character string expected.\n"),"pBurg",5))
    end
    if withF&opt<>"twosided" then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),"pBurg",5,"""twosided"""))
    elseif and(opt<>["onesided","twosided"]) then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),"pBurg",5,strcat(["""onesided""","""twosided"""],",")))
    end
  end
endfunction
