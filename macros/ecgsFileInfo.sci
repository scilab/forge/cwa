//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [fe,Nchannels,Nsamples,t0]=ecgsFileInfo(filein)
  if type(filein)==10 then
    in=mopen(filein,'rb');
  else
    in=filein;
    mseek(0,in,"set")
  end
  if ascii(mget(9,'c',in))<>"ECGScilab" then
    mclose(in)
    error(msprintf(_("%s: Wrong value for input argument #%d: Unexpected file type"),"ecgsFileInfo",1))
  end
  t0=mget(6,"s",in);
  Nchannels=mget(1,'i',in);
  Nsamples=mget(1,'i',in);
  fe=mget(1,'f',in);
  if type(filein)==10 then mclose(in);end
endfunction
