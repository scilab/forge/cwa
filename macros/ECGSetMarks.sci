//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ECGSetMarks(e,d)
//select the mark style for each detection
//e : handle on the polyline
//d : detection type 1:9  
  s=8;
  select d
  case 1 //Rp
    m=6;c=color("red");
  case 2 //Re
    m=7;c=color("red");
  case 3 //To
    m=10;c=color("orange");
  case 4 //Tp
    m=6;c=color("orange");
  case 5 //Te
    m=7;c=color("orange");
  case 6 //Po
    m=10;c=color("blue");
  case 7 //Pp
    m=6;c=color("blue");
  case 8 //Pe
    m=7;c=color("blue");
  case 9 //Qo
    m=9;s=5;c=color("magenta");
  end
  e.mark_style=m;
  e.mark_size_unit='point';
  e.mark_size=s;
  e.mark_background=c;
  e.thickness=1;
  e.mark_foreground=c;
endfunction
