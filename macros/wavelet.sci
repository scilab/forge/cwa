//----------------------------------------------------------------------------
//   Copyright (C) 1995-2004, Christopher Torrence and Gilbert P. Compo
//
//   This software may be used, copied, or redistributed as long as it is not
//   sold and this copyright notice is reproduced on each copy made. This
//   routine is provided as is without any express or implied warranties
//   whatsoever.
//
// Notice: Please acknowledge the use of the above software in any publications:
//    ``Wavelet software was provided by C. Torrence and G. Compo,
//      and is available at URL: http://paos.colorado.edu/research/wavelets/''.
//
// Reference: Torrence, C. and G. P. Compo, 1998: A Practical Guide to
//            Wavelet Analysis. <I>Bull. Amer. Meteor. Soc.</I>, 79, 61-78.
//
// Please send a copy of such publications to either C. Torrence or G. Compo:
//  Dr. Christopher Torrence               Dr. Gilbert P. Compo
//  Research Systems, Inc.                 Climate Diagnostics Center
//  4990 Pearl East Circle                 325 Broadway R/CDC1
//  Boulder, CO 80301, USA                 Boulder, CO 80305-3328, USA
//  E-mail: chris[AT]rsinc[DOT]com         E-mail: compo[AT]colorado[DOT]edu
//----------------------------------------------------------------------------

function [wave,period,Scale,coi] = wavelet(Y,dt,pad,dj,s0,J1,mother,param);
//1D Wavelet transform with optional significance testing
//
//   [wave,period,scale,coi] = wavelet(y,dt,pad,dj,s0,j1,mother,param)
//
//   Computes the wavelet transform of the vector Y (length N),
//   with sampling rate DT.
//
//   By default, the Morlet wavelet (k0=6) is used.
//   The wavelet basis is normalized to have total energy=1 at all scales.
//
//
// INPUTS:
//
//    y = the time series of length N.
//    dt = amount of time between each y value, i.e. the sampling time.
//
// OUTPUTS:
//
//    wave is the WAVELET transform of y. This is a complex array
//    of dimensions (N,J1+1). abs(wave) gives the WAVELET amplitude,
//    atan(imag(wave),real(wave)) gives the WAVELET phase.
//    The WAVELET power spectrum is abs(wave)^2.
//    Its units are sigma^2 (the time series variance).
//
//
// OPTIONAL INPUTS:
// 
// *** Note *** setting any of the following to -1 will cause the default
//               value to be used.
//
//    pad = if set to 1 (default is 0), pad time series with enough zeroes to get
//         N up to the next higher power of 2. This prevents wraparound
//         from the end of the time series to the beginning, and also
//         speeds up the FFT's used to do the wavelet transform.
//         This will not eliminate all edge effects (see COI below).
//
//    dj = the spacing between discrete scales. Default is 0.25.
//         A smaller # will give better scale resolution, but be slower to plot.
//
//    s0 = the smallest scale of the wavelet.  Default is 2*dt.
//
//    j1 = the # of scales minus one. Scales range from s0 up to s0*2^(j1*dj),
//        to give a total of (j1+1) scales. Default is j1 = (LOG2(N dt/s0))/dj.
//
//    mother = the mother wavelet function.
//             The choices are 'MORLET', 'PAUL', or 'DOG'
//
//    param = the mother wavelet parameter.
//            For 'MORLET' this is k0 (wavenumber), default is 6.
//            For 'PAUL' this is m (order), default is 4.
//            For 'DOG' this is m (m-th derivative), default is 2.
//
//
// OPTIONAL OUTPUTS:
//
//    period = the vector of "Fourier" periods (in time units) that corresponds
//           to the SCALEs.
//
//    Scale = the vector of scale indices, given by s0*2^(j*dj), j=0...j1
//            where j1+1 is the total # of scales.
//
//    COI = if specified, then return the Cone-of-Influence, which is a vector
//        of N points that contains the maximum period of useful information
//        at that particular time.
//        Periods greater than this are subject to edge effects.
//        This can be used to plot COI lines on a contour plot by doing:
//
//              contour(time,log(period),log(power))
//              plot(time,log(COI),'k')
//  
  nargin=argn(2)
  if (nargin < 2)
    error('Must input a vector Y and sampling time DT')
  end

  n1 = length(Y);
  if (nargin < 8), param = -1; end
  if (nargin < 7), mother = 'MORLET'; end
  if (nargin < 5), s0 = 2*dt;, end
  if (nargin < 4), dj = 1/4;, end
  if (nargin < 6), J1=fix((log2(n1*dt/s0))/dj); end
  if (nargin < 3), pad = %F;, end
  
 

  //....construct time series to analyze, pad if necessary
  x = matrix(Y - mean(Y),1,-1);
  if pad then
    base2 = fix(log2(n1) + 0.4999);   // power of 2 nearest to N
    x = [x,zeros(1,2^(base2+1)-n1)];
  end
  n = length(x);

  //....construct wavenumber array used in transform [Eqn(5)]
  k = 1:fix(n/2);
  k = k.*((2.*%pi)/(n*dt));
  k = [0., k, -k(fix((n-1)/2):-1:1)];

  //....compute FFT of the (padded) time series
  f = fft(x);    // [Eqn(3)]

  //....construct SCALE array & empty PERIOD & WAVE arrays
  Scale = s0*2.0.^((0:J1)*dj);
  period = Scale;
  wave = zeros(J1+1,n);  // define the wavelet array

  // loop through all scales and compute transform
  for a1 = 1:J1+1
    [daughter,fourier_factor,coi,dofmin]=wave_bases(mother,k,Scale(a1),param);	
    wave(a1,:) = ifft(f.*daughter);  // wavelet transform[Eqn(4)]
  end

  period = fourier_factor*Scale;
  w=[1E-5,1:((n1+1)/2-1)];
  coi = coi*dt*[w,w($:-1:1)];  // COI [Sec.3g]
  wave = wave(:,1:n1);  // get rid of padding before returning
endfunction

function [daughter,fourier_factor,coi,dofmin] = wave_bases(mother,k,Scale,param);
//WAVE_BASES  1D Wavelet functions Morlet, Paul, or DOG
//
//  [DAUGHTER,FOURIER_FACTOR,COI,DOFMIN] = ...
//      wave_bases(MOTHER,K,SCALE,PARAM);
//
//   Computes the wavelet function as a function of Fourier frequency,
//   used for the wavelet transform in Fourier space.
//   (This program is called automatically by WAVELET)
//
// INPUTS:
//
//    MOTHER = a string, equal to 'MORLET' or 'PAUL' or 'DOG'
//    K = a vector, the Fourier frequencies at which to calculate the wavelet
//    SCALE = a number, the wavelet scale
//    PARAM = the nondimensional parameter for the wavelet function
//
// OUTPUTS:
//
//    DAUGHTER = a vector, the wavelet function
//    FOURIER_FACTOR = the ratio of Fourier period to scale
//    COI = a number, the cone-of-influence size at the scale
//    DOFMIN = a number, degrees of freedom for each point in the wavelet power
//             (either 2 for Morlet and Paul, or 1 for the DOG)
//

  mother = convstr(mother);
  n = length(k);
  K=bool2s(k>0)
  select  convstr(mother)
  case 'morlet'
    if (param == -1), param = 6.; end
    k0 = param;
    expnt = -(Scale.*k - k0).^2/2 .*K;
    nrm = sqrt(Scale*k(2))*(%pi^(-0.25))*sqrt(n);    // total energy=N   [Eqn(7)]
    daughter = nrm*exp(expnt);
    daughter = daughter.*K;     // Heaviside step function
    fourier_factor = (4*%pi)/(k0 + sqrt(2 + k0^2)); // Scale-->Fourier [Sec.3h]
    coi = fourier_factor/sqrt(2);                  // Cone-of-influence [Sec.3g]
    dofmin = 2;                                    // Degrees of freedom
  case "paul"
    if (param == -1), param = 4.; end
    m = param;
    expnt = -(Scale.*k).*K;
    nrm = sqrt(Scale*k(2))*(2^m/sqrt(m*prod(2:(2*m-1))))*sqrt(n);
    daughter = nrm*((Scale.*k).^m).*exp(expnt);
    daughter = daughter.*K;     // Heaviside step function
    fourier_factor = 4*%pi/(2*m+1);
    coi = fourier_factor*sqrt(2);
    dofmin = 2;
  case "dog"
    if (param == -1), param = 2.;, end
    m = param;
    expnt = -(Scale.*k).^2 ./ 2.0;
    nrm = sqrt(Scale*k(2)/gamma(m+0.5))*sqrt(n);
    daughter = -nrm*(%i^m)*((Scale.*k).^m).*exp(expnt);
    fourier_factor = 2*%pi*sqrt(2 ./ (2*m+1));
    coi = fourier_factor/sqrt(2);
    dofmin = 1;
  else
    error('Mother must be one of MORLET,PAUL,DOG')
  end

endfunction

