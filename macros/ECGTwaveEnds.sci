// This file is part of the  Cardiovascular Wawes Analysis toolbox
// Copyright (C) 2005 - INRIA - Qinghua Zhang
// Copyright (C) 2014 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [kTe,Ttype] = ECGTwaveEnds(S,varargin)
//TWAVEEND:  T-wave end location
// kTe = ECGTwaveEnds(S, fs, Rpeaks,[,options])
// kTe = ECGTwaveEnds(S, Rpeaks [,options])
//
//   S:       ECG signal (single channel)
//   fs:      sampling frequency
//   Rpeaks : R wave peak indices
// Optional input arguments given by  a sequence of key,value pairs
  
//   "swin":    sliding window width is given as the number of sample points.
//   "mthrld":   morphology threshold specification is either a positive number or a character with 'p' for positive
//            T-wave or 'n' for negative T-wave.
//            positive number gives a threshold used to distinguish mono
//            and biphasic waves: if the ratio of the first peak height by
//            the second peak height if greater than mthrld then the T
//            wave is considered to be monophasic.
// Outputs    
//   kTe: T-wave end indices
// References
//  https://www.researchgate.net/publication/224628140_Robust_and_efficient_location_of_T-wave_ends_in_electrocardiogram
//  https://www.researchgate.net/publication/6646590_An_algorithm_for_robust_and_efficient_location_of_T-wave_ends_in_electrocardiograms
  
// Authors: 
//  - Qinghua Zhang (original Matlab function) twaveend.m  part of the
//         ecgtwave package registered at APP 
//         under the number IDDN.FR.001.430041.000.S.P.2005.000.31230. 
//  - Serge Steer (translation to Scilab and vectorization)
  nin = argn(2);
  if typeof(S)=="sciecg" then
    if nin<2 then
      error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),"ECGTwaveEnds",2,4))
    end
   
    if typeof(S)<>"sciecg" then
      error(msprintf(_("%s: Wrong type for argument %d: sciecg data structure expected.\n"),...
                                      "ECGTwaveEnds",1))
    end
    if size(S,2)<>1 then
      error(msprintf(_("%s: Wrong size for argument %d: a single channel ecg expected.\n"),...
                                      "ECGTwaveEnds",1))
    end
    fs=S.fs;
    S=S.sigs;
    Rpeaks=varargin(1);
    kopt=2;
  else
    if nin<3 then
      error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),"ECGTwaveEnds",3,5))
    end
    if type(S)<>1|~isreal(S) then
      error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n"),...
                     "ECGTwaveEnds",1))
    end
    if and(size(S)<>1)
      error(msprintf(_("%s: Wrong size for argument #%d: Vector expected.\n"),...
                     "ECGTwaveEnds",1))
    end
    S=S(:);
    fs=varargin(1);
    if type(fs)<>1|~isreal(fs)|size(fs,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),...
                     "ECGTwaveEnds",2))
    end
    if fs<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),...
                     "ECGTwaveEnds",2))
    end
    Rpeaks=varargin(2);
    kopt=3;
  end
 
  if type(Rpeaks)<>1|~isreal(Rpeaks)|or(round(Rpeaks)<>Rpeaks) then
    error(msprintf(_("%s: Wrong type for input argument #%d: Matrix of integers expected.\n"),...
                   "ECGTwaveEnds",kopt-1))
  end
  if and(size(Rpeaks)<>1)
    error(msprintf(_("%s: Wrong size for argument #%d: Vector expected.\n"),...
                   "ECGTwaveEnds",kopt-1))
  end
  if or(Rpeaks<=0) then
    error(msprintf(_("%s: Wrong values for input argument #%d: Elements must be positive.\n"),...
                   "ECGTwaveEnds",kopt-1))
  end
  

  //Summation  window size (W) default value
  swin = 0.128*fs;// in number of samples
  mthrld = 6;
  debug_flag=%f;Te_ann = []; //for debuging purpose
  for k=kopt:2:size(varargin)
    select varargin(k)
    case "swin"
      if varargin(k+1)<>[] then 
        swin=varargin(k+1);
      end
    case "mthrld"
      if varargin(k+1)<>[] then 
        mthrld=varargin(k+1);
      end
    case "Ttype"
      if varargin(k+1)<>[] then mthrld=varargin(k+1);end
    case "debug"
      debug_data=varargin(k+1);
      if typeof(debug_data)=="st" then
        fn=fieldnames(debug_data)
        for fn=fieldnames(debug_data)'
          if and(fn<>["name","sigsel","Sigs","Te_ann"]) then
            error(msprintf(_("%s: Wrong value for input argument #%d: unexpected field %s.\n"),...
                       "ECGTwaveEnds",k+1,fn))
          end
        end
      elseif debug_data<>[] then
        error(msprintf(_("%s: Wrong type for input argument #%d: A struct or [] expected.\n"),...
                       "ECGTwaveEnds",k+1))
      end
      debug_flag=%t
    end
  end
    
  
  N=length(S);
  //length of  smoothing window p (used to compute \bar{s}_k  p<<W
  ptwin = ceil(0.016*fs); //0.016s

  nT = length(Rpeaks);
  if Rpeaks(nT)+200 > N then  nT = nT-1;end

  kTe = zeros(nT,1);
  Ttype=emptystr(nT,1);
  //length of the R-R intervals
  RR=[diff(Rpeaks) 0.8*fs];//0.8s
 
  //T-wave end search interval parameters: 
  //   The T wave peak and the T wave end for the kth beat are supposed to
  //   be in the interval [Leftbound(k) rightbound(k)]

  if %f then
    //The original code
    //   Compute boundaries for  R-R interval lengths greater than 0.88s
    al = 0;
    bl = 0.28*fs;//0.28s
    ar = 0.2;
    br = 0.404*fs;//0.404s
    Leftbounds = floor(al*RR+bl);//ka
    Rightbounds = ceil(ar*RR+br);//kb
    
    //    Compute boundaries for  R-R interval lengths less than 0.88s
    k=find(RR<0.88*fs);//0.88s
    if k<>[] then
      al = 0.15;
      bl = 0.148*fs;//0.148s
      ar = 0.7;
      br = -0.036*fs;//-0.036s
      Leftbounds(k) = floor(al*RR(k)+bl);//ka
      Rightbounds(k) = ceil(ar*RR(k)+br);//kb
    end
  else
    //The parameters above have been optimized over the QTDB database.
    Leftbounds = floor(0.089*sqrt(RR*fs)+0.139*fs);
    Rightbounds = ceil(0.719*sqrt(RR*fs)-0.191*fs);
    //protect against very short RR
    Rightbounds = max(Leftbounds+5,Rightbounds);
  end
  Leftbounds = Rpeaks+Leftbounds;
  Rightbounds = Rpeaks+Rightbounds;
  
  //Insure  Rightbound being lower than the next peak index
  Rightbounds(1:$-1)=min(Rightbounds(1:$-1),Rpeaks(2:$));
  //Insure signal is long enough to be able to compute the indicator function
  Rightbounds($)=min(Rightbounds($),N-ptwin);
  Leftbounds($)=min(Leftbounds($),Rightbounds($)-2);
  for knR=1:nT
    kR=Rpeaks(knR);
    leftbound=Leftbounds(knR)
    leftbound=max(leftbound,swin);
    rightbound=Rightbounds(knR)//+30;
    // Compute the area indicator for all values of t
    Tval=conv(S(leftbound-swin+1:rightbound),ones(1,swin),'valid')-...
         conv(S(leftbound-ptwin:rightbound+ptwin),ones(1,2*ptwin+1)*(swin/(2*ptwin+1)),'valid');
    [kTe(knR),Ttype(knR)]=QZ_decide()
    if debug_flag then debugTwaveEnd();end
  end

endfunction

function [kTe,typ]=QZ_decide()
//Handling different morphological forms. arguments passed by the
//calling context.
  typ=''
  [dum, maxind] = max(Tval);
  [duminv, maxindinv] = max(-Tval);

  //mthrld=4;
  if mthrld=="p" then //positive T wave only
    kTe = maxind + leftbound - 1;
    Ttype="p";
  elseif mthrld=="n" then //negative T wave only
    kTe = maxindinv + leftbound - 1;
    Ttype="n";
  else
    if maxind<maxindinv  then //onde positive avant la négative t'<t''
      leftind = maxind;
      rightind = maxindinv;
      leftdum = dum; // |A(t')|
      rightdum = duminv; // |A(t'')|
      typ="pn";
    else //onde négative avant la positive  t''<t'
      leftind = maxindinv;
      rightind = maxind;
      leftdum = duminv; // |A(t'')|
      rightdum = dum;  //  |A(t')|
      typ="np";
    end
    if leftdum>mthrld*rightdum then
      // |A(t')| differs to |A(t'')|  ==> monophasic T wave
      kTe = leftind + leftbound - 1;
    else
      // |A(t')| is similar to |A(t'')| ==> biphasic T wave
      kTe = rightind+ leftbound - 1;
    end
  end
endfunction

function debugTwaveEnd()
//used by ECGTwaveEnds for debugging purposes,
  
  if knR>=size(Rpeaks,'*') then return;end
  kTe=kTe(knR);

  Te_ann=[];name=""
  Sigs=S;sigsel=1;
  if debug_data<>[] then
    fn=fieldnames(debug_data)
    for i=1:size(fieldnames(debug_data),'*')
      execstr(fn(i)+"=debug_data(fn(i))");
    end
  end
  k_ann=[];
  if Te_ann<>[] then
    k_ann=find((Te_ann*fe>Rpeaks(knR))&(Te_ann*fe<Rpeaks(knR+1)));
    if k_ann<>[] then kTe_ann=Te_ann(k_ann)*fe;end
  end
  if k_ann==[] then return,end
  fig=gcf();fig.axes_size = [500,300]
  drawlater
  clf;subplot(211);ax1=gca();ax1.font_style=9;ax1.font_size=2;
  t=(Rpeaks(knR)-10:Rpeaks(knR+1)+10)';
//  xinfo(msprintf("RR=%.2g",(Rpeaks(knR+1)-Rpeaks(knR))/fe))
  RR=(Rpeaks(knR+1)-Rpeaks(knR))/fe;
  Sigs=Sigs.sigs;//sigsel=1;name=""
  plot(t,Sigs(t,:)),
  plot([leftbound-swin leftbound,rightbound]',Sigs([leftbound-swin leftbound,rightbound],sigsel),'+r');
  if k_ann<>[] then
    plot(kTe_ann,Sigs(kTe_ann,sigsel),'og');
    ax1.title.text=msprintf("%s channel:%d, #peak:%d, #annot:%d, RR=%.2gs",name,sigsel,knR,k_ann,RR)
  else
    ax1.title.text=msprintf("%s channel:%d, #peak:%d, RR=%.2gms",name,sigsel,knR,RR)
  end
  plot(kTe,Sigs(kTe,sigsel),'or')
  xgrid()
  ax1.margins(3:4)=[0.2 0]
  ax1.x_ticks.labels=emptystr(ax1.x_ticks.labels)

  subplot(212);ax2=gca();ax2.font_style=9;ax2.font_size=2;
  plot((leftbound:rightbound)',Tval),plot(kTe,Tval(kTe-leftbound+1),'or')
  if k_ann<>[]&kTe_ann-leftbound+1>0&kTe_ann-leftbound+1<=size(Tval,'*') then
    plot(kTe_ann,Tval(kTe_ann-leftbound+1),'og')
  end
  ax2.data_bounds(:,1)=ax1.data_bounds(:,1); xgrid()
  ax2.margins(3:4)=[0 0.2]
  drawnow
  id=fig.figure_id
  addmenu(id,_("Actions"),["Next","Export","Stop"],list(2,"db_actions"))
  realtimeinit(0.1);count=0;
  global %db_r;%db_r=0
  while %db_r==0,
    r=xgetmouse();
    select r(3)
    case 32 then //" "
      %db_r=1
    case 113 then //"q"
      %db_r=2
    case 115 then //"s"
      %db_r=2
    end
          
    realtime(count),
  end
  if %db_r==1 then
    clearglobal %db_r
  else
    clearglobal %db_r
    debug_flag=resume(%f)
  end
  
endfunction
function db_actions(k,win),
  global %db_r
  select k
  case 1 then
    %db_r=1
  case 2 then
//    xs2svg(fig,name+"_"+sci2exp(sigsel)+"_"+string(knR)+".svg")
    xs2pdf(fig,"tests/Latex/"+ strsubst(name,"tests/qtdb","img")+"_"+sci2exp(sigsel)+"_"+string(knR)+".pdf")
    %db_r=0
  case 3 then
    %db_r=2
  end

endfunction
  
