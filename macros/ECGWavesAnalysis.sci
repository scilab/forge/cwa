//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [duration,kstart]=ECGWavesAnalysis(loc,from,to)
//computes length of speficied QRS complex sements
//fe : sampling frequency of the ECG signal
//loc: 9 rows array produced by ECGDetection  
//from a character string, possible values:   Rp Re To Tp Te Po Pp Pe Qo 
//to a character string, possible values:   Rp Re To Tp Te Po Pp Pe Qo 
//duration vector of the lengths of the selected QRS segment (in number
//of samples)
//kstart vector of the index of the first sample of segments

  keys=["Rp","Re","To","Tp","Te","Po","Pp","Pe","Qo"];
  // Intervalles intra battements 
  ifrom=find(keys==from)
  ito=find(keys==to)
  if ifrom<ito then
    kstart=loc(ifrom,:);
    kend=loc(ito,:)
  elseif  ifrom>ito then
    kend=loc(ifrom,:);
    kstart=loc(ito,:)
  else
    kstart=loc(ifrom,1:$-1);
    kend=loc(ifrom,2:$);
  end
  kstart(kstart<0)=%nan;
  kend(kend<0)=%nan;
  duration=(kend-kstart);
endfunction
