//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function S=readEcgsFile(filein,from,N)
  in=mopen(filein,'rb');
  if ascii(mget(9,'c',in))<>"ECGScilab" then
    mclose(in)
    error(msprintf(_("%s: Wrong value for input argument #%d: Unexpected file type"),"readEcgsFile",1))
  end
  t0=mget(6,"s",in);
  Nchannels=mget(1,'i',in);
  Nsamples=mget(1,'i',in);
  fs=mget(1,'f',in);
  if argn(2)<2 then 
    from=1
  else
    if from<=0|from>Nsamples then
      mclose(in)
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in [%d %d].\n"),"readEcgsFile",2,1,Nsamples))
    end
  end
  if argn(2)<3 then 
    N=Nsamples-from+1
  else
    if N<=0|N>Nsamples-from+1 then
      mclose(in)
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in [%d %d].\n"),"readEcgsFile",3,1,Nsamples-from+1))
    end
  end
  //skip first samples
  if from>1 then
    pos=mtell(in)
    mseek_big(pos+(from-1)*Nchannels*8,in)
  end
  S=sciecg(fs,matrix(mget(Nchannels*N,'d',in),Nchannels,-1)');
  mclose(in)
endfunction
