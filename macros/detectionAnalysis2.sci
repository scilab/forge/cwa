//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [d,M,S]=detectionAnalysis2(d1,d2,nclass)
  if argn(2)<3 then nclass=20;end
  mn1=min(d1);mx1=max(d1);
  mn2=min(d2);mx2=max(d2);
 //calcul de l'histogramme par rapport à la premiere variable
  dd1=linspace(mn1,mx1,nclass);pas1=dd1(2)-dd1(1);
  dd2=linspace(mn2,mx2,nclass);pas2=dd2(2)-dd2(1);
  occ=dsearch2D(d1,d2,dd1,dd2);

  M=zeros(1,nclass-1);
  S=zeros(1,nclass-1);
  x=dd2(2:$)-pas2/2;
  for k=1:nclass-1
    y=occ(k,:)/sum(occ(k,:));
    [sigma,mu,e]=fit_gaussian(x,y,60,90);
    M(k)=mu;
    S(k)=sigma;
  end 
  S=S*pas2
  d=dd1(2:$)-pas1/2;
endfunction
