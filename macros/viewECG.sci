//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function viewECG(S,fs,tit)
  if typeof(S)<>"sciecg" then
    if type(S)<>1|~isreal(S) then
      error(msprintf(_("%s: Wrong type for argument %d: a real array or an sciecg data structure expected .\n"),...
                     "viewECG",1))
    end
    if type(fs)<>1|~isreal(fs)|size(fs,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),...
                     "ECGRpeaks",2))
    end
    if fs<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),...
                     "ECGRpeaks",2))
    end
    if argn(2)<3 then tit=[];end
  else
    if argn(2)>=2 then 
      tit=fs
    else
      tit=[]
    end
    fs=S.fs
    S=S.sigs
  end

  [nt,nvoies]=size(S);
  t=(0:nt-1)'/fs;
  f=gcf();
  clf;drawlater()
  ha=0.8/nvoies
  ya=0.05
  for k=1:nvoies
    a=newaxes();
    a.axes_bounds=[0 ya 1 ha*0.95];
    a.margins(3:4)=0;
    ya=ya+ha;
    plot(t,S(:,k))
    a.grid=color("lightgray")*[1 1];
    if k<nvoies then
      a.x_ticks.labels=emptystr(a.x_ticks.labels);
    end
    if k==1&tit<>[] then title(tit);end
    ylabel("mV")
  end
  xlabel(_("Time (s)"))
  drawnow()
endfunction
