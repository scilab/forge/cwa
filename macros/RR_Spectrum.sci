function [f,p]=RR_Spectrum(RR,df)
  select typeof(RR)
  case "constant" then  //RR given
    if and(size(RR)>1) then
      error(msprintf(_("%s: Wrong size for argument #%d: Vector expected.\n"),"RR_Spectrum",1))
    end
  case "sciecg" then
    S=RR;clear RR;
    if size(S,2)>1 then S=SynthesisECG(S);end
    kRp = ECGRpeaks(S);
    RR=diff(kRp)/S.fs;//in seconds
  else
    error(msprintf(_("%s: Wrong type for input argument #%d: ''%s'' or ''%s'' expected.\n"),"RR_Spectrum",1,"constant","sciecg"))
  end
  t=cumsum(RR);t=t-t(1);
  ofac=4
  if argn(2)==2 then
    ofac= max(1,1.0/(max(t)*df));
  end
  [f,p]=Lomb(t,RR,ofac,2)
endfunction
