function SA_Settings()
  h=gcbo
  if execstr("v="+h.string,"errcatch")<>0 then
    h.ForegroundColor=[1 0 0]
    return
  else
    h.ForegroundColor=[0 0 0]
    key=h.userdata
    fig=h.parent.parent
    fig_ud=fig.user_data
    fig_ud(key)=v
    set(fig,"user_data",fig_ud)
    SA_compute(fig)
    SA_draw(fig)
  end
endfunction
