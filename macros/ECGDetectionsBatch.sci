//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [LOC,succeed]=ECGDetectionsBatch(f,varargin)
//ECGDetectionsBatch localize caracteristic points along a single channel ECG
//stored in the ECGScilab file which name is givene by the argument f.
//The caracteristics points looked for are
//       1: Rp the R wave peak locations
//       2: Re the R wave end locations
//       3: To the T wave  onset locations
//       4: Tp the T wave peak locations
//       5: Te the T wave end locations
//       6: Po the P wave onset locations
//       7: Pp the P wave peak locations
//       8: Pe the P wave end locations
//       9: Qo the Q wave onset locations
//The index of these caracteristic points are returned in the LOC
//   variable which is a 9 rows array. The ith column as the followind content:
//   [kRp;kRe;kTo;kTe;kTp;kPo;kPp;kPe;kQo] where kXy is the index of the Xy
//   caracteristic point for the ith beat.
//The missing detections are indicated by a negative value of the
//associated index.
//For each column we have kRp<kRe<kTo<kTe<kTp<kPo<kPp<kPe<kQo assuming
//all locations have been detected for the corresponding beat.


  in=mopen(f,"rb");
  if ascii(mget(9,'c',in))<>"ECGScilab" then
    mclose(in)
    error(msprintf(_("%s: Wrong value for input argument #%d: Unexpected file type"),...
                   "ECGDetectionBatch",1))
  end
  

  
  LOC=[];
  t0=mget(6,"s",in);
  Nchannels=mget(1,'i',in);
  if Nchannels<>1 then
    mclose(in)
    error(msprintf(_("%s: Wrong value for input argument #%d: Single channel ECG file expected\n"),...
                   "ECGDetectionBatch",1))
  end
  Nsamples=mget(1,'i',in);
  fs=mget(1,'f',in);
  
  BlockLength=150000; 
  for k=1:2:size(varargin)
    if varargin(k)=="BlockLength" then
      BlockLength=varargin(k+1)
      break
    end
  end

  winH=mywaitbar("open",_("Processing detection"));
  //Boucle sur les blocs
  Nb=ceil(Nsamples/BlockLength);
  Index=0;
  pastloc=[];
  succeed=%t
  S_mem=[];
  try
    for b=1:Nb
      l_mem=size(S_mem,'*');
      S=[S_mem;matrix(mget(Nchannels*BlockLength,'d',in),-1,1)];
      [loc,loc_last_rpeak]=ECGDetections(S,fs,varargin(:));
      S_mem=S((loc(1,$)+loc_last_rpeak)/2:$);
      if loc<>[] then
        good=find(loc<>-1);
        loc(good)=loc(good)+Index-l_mem;
        LOC=[LOC loc];
      end
      Index=Index+BlockLength;
      mywaitbar("update",b/Nb,winH);
    end 
   catch
     succeed=%f
     mprintf(_("%s: Unexpected error during detection in block starting at sample %d:\n%s\n"),...
             "ECGDetectionsBatch",b*BlockLength,lasterror())
     pause
   end
  mclose(in)
  mywaitbar("close",winH);
endfunction
