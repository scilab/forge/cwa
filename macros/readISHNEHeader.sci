// This file is part of the Cardiovascular Wawes Analysis toolbox
// Copyright (C)  - University of Rochester Medical Center (URMC). 
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function header=readISHNEHeader(fid);
//fid: file descriptor of an ISHNE file 
//header : a struct which contains the following fields:
//    Var_length_block_size: a scalar
//    Sample_Size_ECG: a scalar
//    Offset_ECG_block: a scalar, the number of bytes in the header
//    First_Name: a character string, the subject first name
//    Last_Name: a character string, the subject last name
//    ID: a character string
//    Sex: a scalar, 0:unknown, 1:male, 2:female
//    Type: a scalar, 0:unknown, 1:caucasian, 2:black, 3:oriental
//    Birth_Date: a vector, the subject birth date [day month year]
//    Record_Date: a vector, the record date [day month year]
//    File_Date: a vector, the file date [day month year]
//    Start_Time: a vector, give the record start time [hour,min,sec]
//    nbLeads: a scalar, the number of leads
//    Lead_Spec: a 1 by 12 vector, the specification of each lead 
//               Unknown : 0
//               Generic bipolar: 1
//               X bipolar: 2
//               Y bipolar: 3
//               Z bipolar: 4
//               I to Vf  : 5 to 10
//               V1 to V6 : 11 to 16
//               ES: 17
//               AS: 18
//               AI: 19
//    Lead_Qual: a 1 by 12 vector, the  lead quality score
//               0:unrated,1:good
//               2:intermittent noise (<10% of the record length),
//               3:frequent noise (>10% of the record length)
//               4:lead disconnection <10% of the record length
//               5:lead disconnection >10% of the record length
//    Resolution: a 1 by 12 vector, the  lead amplitude resolutions in nV
//    Pacemaker: a scalar, The type of pacemaker
//               0: no pacemaker
//               1: unknown pacemaker type
//               2: single chamber unipolar
//               3: dual chamber unipolar
//               4: single chamber bipolar
//               5: dual chamber  bipolar 
//    Recorder: a character string, type of recorder (analog/digital)
//    Sampling_Rate: The sampling rate in Hz
//    Proprietary: a character string, the proprietary identification
//    Copyright a character string, the file copyright
//
//This file is derived from the Matlab function found at 
//  http://thew-project.org/code_library.htm
  
  opened=type(fid)==1

  if ~opened then
     fid=mopen(fid,'rb');
     magicNumber = ascii(mget(8,"uc",fid));
     if and(magicNumber<>["ISHNE1.0" "ANN  1.0"]) then 
       disp(magicNumber)
      error("The given file is not a ISHNE file") 
     end
  end
  // get checksum
  header.checksum = mget( 1,"us",fid);
  
  //read header
  header.Var_length_block_size = mget( 1,"ui",fid);
  header.Sample_Size_ECG = mget( 1,"ui",fid);//size in samples of the ECG	
  Offset_var_lenght_block = mget( 1,"ui",fid);
  header.Offset_ECG_block = mget( 1,"ui",fid);
  File_Version = mget(1,"us",fid);
  t = mget( 40,"c",fid); 
  header.First_Name =ascii(t(1:find(t==0,1)-1));//subject first name
  t = mget( 40,"c",fid); 
  header.Last_Name =ascii(t(1:find(t==0,1)-1));//subject last name
  t = mget( 20,"c",fid); 
  header.ID =ascii(t(1:find(t==0,1)-1))
  header.Sex = mget( 1,"us",fid);
  header.Type = mget( 1,"us",fid);
  header.Birth_Date = mget( 3,"us",fid);
  header.Record_Date = mget( 3,"us",fid);
  header.File_Date = mget( 3,"us",fid);
  header.Start_Time = mget( 3,"us",fid);
  header.nbLeads = mget( 1,"us",fid);
  header.Lead_Spec = mget( 12,"us",fid);	
  header.Lead_Qual = mget( 12,"us",fid);

  header.Resolution = mget( 12,"us",fid); //vector number of nV	for each lead
  header.Pacemaker = mget( 1,"us",fid);	//
  t= mget( 40,"c",fid);
  header.Recorder=ascii(t(1:find(t==0,1)-1)) //type of recorder (analog/digital)
  header.Sampling_Rate = mget( 1,"us",fid);//sampling rate in Hz	
  t = mget( 80,"c",fid);
  header.Proprietary = ascii(t(1:find(t==0,1)-1))
  t = mget( 80,"c",fid);
  header.Copyright= ascii(t(1:find(t==0,1)-1))
  Reserved = mget( 88,"c",fid);
  if ~opened then mclose(fid);end
endfunction
