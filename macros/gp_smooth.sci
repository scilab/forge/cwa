function ys=gp_smooth(x,y,wc)
//http://downloads.hindawi.com/journals/cmmm/2012/578785.pdf
  sigma2=(sqrt(2)-1)*(2*tan(wc*%pi/2))^(-4)
  N=size(x,"*")-2
  ij=[(1:N)'.*.ones(3,1),ones(N,1).*.(0:2)'+(1:N)'.*.ones(3,1)];
  if argn(2)==2 then //regularly spaced
    sigma=y,y=x;
    V=ones(N,1).*.[1;-2;1]
  else
    x=matrix(x,1,-1);
    id=2:$-1;
    idp1=3:$;
    idm1=1:$-2;;
    V=2.0./[(x(idm1)-x(idp1)).*(x(id)-x(idp1))
            -(x(idm1)-x(id)).*(x(id)-x(idp1))
            (x(idm1)-x(id)).*(x(idm1)-x(idp1))];
    V=V/V(1,1)
  end
  D2=sparse(ij,V(:));
  [D2,P]=spchol((speye(N+2,N+2)+sigma2*(D2'*D2)));
  ys=matrix(P*(D2'\(D2\(P'*y(:)))),size(y));
endfunction
