//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function viewVCG1(vcg)
  [nt,n]=size(vcg);
  nc=64;//nombre de couleurs
  if and(n<>[2 3]) then error("Argument must have 2 or 3 columns");end
  
  k=1;x=[vcg(1:$-1,k) vcg(2:$,k)]';
  k=2;y=[vcg(1:$-1,k) vcg(2:$,k)]';
  if n==3 then k=3;z=[vcg(1:$-1,k) vcg(2:$,k)]';end
  e=sqrt(sum(diff(vcg,1,1).^2,2))
  e=e-min(e);
  e=(e/max(e))*(nc-1)+1;
  
  f=gcf();clf;
  f.color_map=jetcolormap(nc)
  a=gca();
  a.axes_visible="on";
  a.box='on';
  a.grid=ones(1,3)*color("lightgray");
  a.cube_scaling='on';

  if n==3 then
    a.data_bounds=[min(x) min(y) min(z);max(x),max(y),max(z)];
    a.view="3d";
    a.isoview='on'
    xsegs(x,y,z,e)
  else
    a.data_bounds=[min(x) min(y);max(x),max(y)];
    a.isoview='on'
    xsegs(x,y,e)
  end
endfunction
