//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function Ax=MultiChannelPlot(t,varargin)
  na=size(varargin)
  colors=[color('blue'),color('magenta'),color('black'),color('red'),color('orange'),color('green')];
  grid_color=color("gray");
  font_style=6;
  font_size=1;  
  Title=[];
  if typeof(varargin(na))=="st" then
    legalfields=["curve_colors"  "grid_color" "title" "font_style" "font_size"]
    options=varargin(na)

    fn=fieldnames(options)
    for k=1:size(fn,'*')
      select fn(k)
      case "curve_colors" then
        colors=options.curve_colors
      case "grid_color" then
        grid_color=options.grid_color
      case "title" then
        Title=options.title
      case "font_style" then
        font_style=options.font_style
      case "font_size" then
        font_size=options.font_size
      else
       error(msprintf(_("%s: Wrong value for argument %d: Struct fields must be in {%s}\n"),...
                      "MultiChannelPlot",na+1,strcat(legalfields,','))) 
      end
    end
    na=na-1
  end
  
  if modulo(na,2)<>0 then 
    error(msprintf(_("%s: Wrong number of arguments : Each curve must be described by a pair of arguments.\n"),...
                   "MultiChannelPlot"))
  end
  nplot=na/2;
  nt=size(t,'*');t=t(:);
  for k=1:2:na
    signal=varargin(k);
    label=varargin(k+1);
    if type(signal)<>1|~isreal(signal) then
      error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n"),...
                   "MultiChannelPlot",k+1))
    end
    if and(size(signal)>1) then 
      if size(signal,1)<>nt then
        error(msprintf(_("%s: Wrong size for argument %d: A %d column expected.\n"),...
                   "MultiChannelPlot",k+1,nt))
      end
    else
      if size(signal,'*')<>nt then
        error(msprintf(_("%s: Wrong size for input argument #%d: A %d elements array expected.\n"),...
                       "MultiChannelPlot",k+1,nt))
      end

      varargin(k)=signal(:)
    end
   
    
    if type(label)<>10|size(label,'*')<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A character string expected.\n"),...
                       "MultiChannelPlot",k+2))
    end

  end
  o=0.05; //reserve space for title
  h=(1-2*o)/nplot;
  low_margin=0.05;
  nc=size(colors,'*')
  clf;fig=gcf();
  idraw=fig.immediate_drawing;
  fig.immediate_drawing = "off";
  tbounds=[min(t);max(t)]
  Ax=[]
  for kplot=1:nplot
    signal=varargin(2*kplot-1);
    if kplot==1 then 
      a=gca(), 
      if Title<>[] then a.title.text=Title;end
    else  
      a=newaxes();
    end
    a.axes_bounds=[0,o+(kplot-1)*h,1,h];
    a.tight_limits="on";
    a.margins(4)=low_margin;
    a.grid(1:2)=grid_color;
    a.font_style=font_style
    a.font_size=font_size
    
    mny=min(signal)
    mxy=max(signal)
    dy=(mxy-mny)/10;
    a.data_bounds=[tbounds [mny-dy;mxy+dy]]
    a.y_label.text=varargin(2*kplot);
    a.axes_visible='on';
    a.clip_state="clipgrf";
    for i=1:size(signal,2)
      xpoly(t,signal(:,i));
      p=gce();
      p.foreground=colors(modulo(i-1,nc)+1);
    end
    if kplot<>nplot then
      a.x_ticks.labels=emptystr(a.x_ticks.labels)
    else
       a.x_label.text=_("time (s)")
    end
    Ax=[Ax a]
  end
  fig.event_handler="MultiChannelPlotHandler";
  fig.event_handler_enable='off';

  step=round((tbounds(2)-tbounds(1))/10);
  ud=fig.user_data;
  if typeof(ud)<>"st" then
    ud=struct();
  end
  ud.type="MultiChannelPlot";
  ud.colors=colors;
  ud.scroll=[0,step,tbounds']
  set(fig,"user_data",ud);
  submenus=[_("Set scroll length"); _("Scroll On");_("Scroll Off")];
  delmenu(fig.figure_id,_("&Scroll"))
  addmenu(fig.figure_id,_("&Scroll"),submenus,list(2,"MultiChannelScrollMenu"))
  fig.immediate_drawing = idraw;
  
endfunction


function MultiChannelPlotHandler(win, x, y, ibut)
//Utilitary function for MultiChannelPlot function
  if and(ibut<>[37 39 113]) then return,end
  fig=get_figure_handle(win)
  fig.event_handler_enable='off';
  ud=fig.user_data;
  zoom_box=[]
  C=fig.children;
  C=C(C.type=="Axes");
  drawlater()
  select ibut
 
  case 37 then //scroll left
    l=ud.scroll(2);
    count=ud.scroll(1);
    xbounds=ud.scroll(3:4);

    if count>0 then
      for k=1:size(C,'*')
        ax=C(k);
        P=ax.children(ax.children.type=="Polyline")
        if P<>[] then
          zb([1 3])=xbounds(1)+(count-1)*l+[0 l];
          zb([2 4])=selection_y_bounds(P,zb([1 3]))
          ax.zoom_box=zb;
          if ax.auto_ticks(1)=="off" then
            ax.auto_ticks(1)="on";//used to recompute grids positions 
            ax.x_ticks.labels=emptystr(ax.x_ticks.labels)
          end
        end
      end
      ud.scroll(1)=count-1
    end
  case 39 then //scroll right
    l=ud.scroll(2);
    count=ud.scroll(1);
    xbounds=ud.scroll(3:4);
    if xbounds(1)+(count+1)*l<xbounds(2) then
      for k=1:size(C,'*')
        ax=C(k)
        P=ax.children(ax.children.type=="Polyline")
        if P<>[] then
          zb([1 3])=xbounds(1)+(count+1)*l+[0 l];
          zb([2 4])=selection_y_bounds(P,zb([1 3]))
          ax.zoom_box=zb;
          if ax.auto_ticks(1)=="off" then
            ax.auto_ticks(1)="on";//used to recompute grids positions 
            ax.x_ticks.labels=emptystr(ax.x_ticks.labels)
          end
        end
      end
      ud.scroll(1)=count+1
      
    end
  case 113 then //initial view
    drawlater(); 
    for k=1:size(C,'*')
      ax=C(k)
      ax.zoom_box=[];
      if ax.auto_ticks(1)=="off" then
        ax.auto_ticks(1)="on";//used to recompute grids positions 
        ax.x_ticks.labels=emptystr(ax.x_ticks.labels)
      end
    end
    drawnow()
    ud.scroll(1)=0
  end
  drawnow()
  set(fig,"user_data",ud)
  fig.event_handler_enable='on';
endfunction

function ax=findSubwin(x,y,fig)
  sz=fig.axes_size;
  ax=[];
  C=fig.children(fig.children.type=="Axes");
  for k=1:size(C,'*')
    ax=C(k)
    bounds=ax.axes_bounds.*[sz sz]
    if x>=bounds(1)&x<(bounds(1)+bounds(3))&y>=bounds(2)&y<(bounds(2)+bounds(4)) then
      break
    end
  end
endfunction


function y_bounds=selection_y_bounds(P,x_bounds)
  ymin=%inf
  ymax=-%inf
  for k=1:size(P,'*')
    d=P(k).data
    sel=find(d(:,1)>=x_bounds(1)&d(:,1)<=x_bounds(2))
    ymin=min(ymin,min(d(sel,2)))
    ymax=max(ymax,max(d(sel,2)))
  end
  ymin=max(ymin,ax.data_bounds(1,2))
  y_bounds=[max(ymin,ax.data_bounds(1,2)),min(ymax,ax.data_bounds(2,2))];
endfunction

function MultiChannelScrollMenu(k,win)
//Utilitary function for MultiChannelPlot function
  fig=get_figure_handle(win)
  scf(fig)
  select k
  case 1  then //set Scroll length
    C=fig.children;
    C=C(C.type=="Axes");
    ud=fig.user_data;
    count=ud.scroll(1);
    step=ud.scroll(2);
    [ok,l]=uigetlength(_("Give the length of the scrolling window"),step) 
    if ~ok then return,end
    ud.scroll(1:2)=[floor(count*step/l),l];
    set(fig,"user_data",ud);
  case 2  then //Scroll On
    ud=fig.user_data;
    fig.event_handler_enable='off';
    fig.event_handler="MultiChannelPlotHandler";
    fig.event_handler_enable='on';
    xinfo(_("select time window with left and right directionnal arrows, q for full view"))
  case 3  then //Scroll Off
    fig.event_handler_enable='off';
     xinfo("")
  end
endfunction
