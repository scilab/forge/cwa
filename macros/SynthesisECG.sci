//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function S=SynthesisECG(S)
  if typeof(S)<>"sciecg" then
    error(msprintf(_("%s: Wrong type for argument %d: sciecg data structure expected.\n"),...
                   "SynthesisECG",1))
  end
  S.sigs=sqrt(sum(S.sigs.^2,2)/size(S,2));
endfunction
