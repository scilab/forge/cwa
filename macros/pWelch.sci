function [PSD,F]=pWelch(x,varargin);
  //PSD,F]=pWelch(x,windowl,noverlap,nf,fs,opt)
  if argn(2)<1 then 
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),"pWelch",1,6))
  end
  if type(x)<>1|and(size(x)>1)|~isreal(x) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A  real vector expected.\n"),"pWelch",1))
  end
  x=x(:);
  
  [w,sec_step,F,pad,opt,scal]=pWelch_options(varargin)
  windowl=size(w,'*')
  
  wpower=w'*w;//the window energy
  
  nsecs=int((size(x,"*")-windowl+sec_step)/sec_step);
  if nsecs<=0 then
    PSD=[]
    f=[]
    return
  end
  ind=1:windowl;
  PSD=0;
  for k=1:nsecs
    xd=x(ind+(k-1)*sec_step);
    xe=w.*(xd-mean(xd));
    fx=fft([xe;pad]);
    PSD=PSD+real(fx.*conj(fx));
  end
  
  PSD=PSD/(nsecs*wpower*scal);

  if opt=="onesided" then
    n=ceil((size(F,'*')+1)/2)
    F=F(1:n)
    PSD=2*PSD(1:n)

  end
endfunction

function [w,sec_step,F,pad,opt,scal]=pWelch_options(optlist)
  //windowl,noverlap,nf,fs,opt,scale
  withF=%f
  
  nopt=size(optlist)
  if nopt<1|optlist(1)==[] then
    windowl=int(size(x,'*')/8)
    w=window("hm",windowl)';
  else
    windowl=optlist(1)
    if type(windowl)<>1|~isreal(windowl) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A  real array expected.\n"),"pWelch",2))
    end
    if size(windowl,'*')>1 then
      w=windowl(:)
      windowl=size(w,'*')
    else
      w=window("hm",windowl)';
    end
  end
  if nopt<2|optlist(2)==[] then
    noverlap=round(windowl/2)
  else
    noverlap=optlist(2);
    if type(noverlap)<>1|~isreal(noverlap)|size(noverlap,'*')>1|int(noverlap)<>noverlap then
      error(msprintf(_("%s: Wrong type for input argument #%d: A positive integer expected.\n"),"pWelch",3))
    end
  end
  if nopt<4|optlist(4)==[] then
    fs=1
    scal=2*%pi
  else
    
    fs=optlist(4)
    if type(fs)<>1|size(fs,'*')<>1|fs<=0 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A positive real expected.\n"),"pWelch",5))
    end
    scal=fs
  end
  
  if nopt<3|optlist(3)==[] then
    nf=max(256,2^nextpow2(windowl))
    F=(0:(nf-1))*(fs/nf);
  else
    nf=optlist(3)
    if type(nf)<>1|or(nf<0) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A positive real array expected.\n"),"pWelch",4))
    end
    if size(nf,'*')==1 then
      F=(0:(nf-1))*(fs/nf)
    else
      F=nf
      nf=size(F,'*')
      withF=%t
    end
  end
  
  if nopt<5 then 
    if withF then opt="twosided"; else opt="onesided";end
  else
    opt=optlist(5)
    if type(opt)<>10|size(opt,'*')<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A character string expected.\n"),"pWelch",5))
    end
    if withF&opt<>"twosided" then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),"pWelch",5,"""twosided"""))
    elseif and(opt<>["onesided","twosided"]) then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),"pWelch",5,strcat(["""onesided""","""twosided"""],",")))
    end
  end
  F=(0:(nf-1))'*(fs/nf);
  sec_step=windowl-noverlap
  if windowl<nf then
    pad=zeros(nf-windowl,1)
  else
    pad=[]
  end
endfunction
