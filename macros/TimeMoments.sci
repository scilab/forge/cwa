// This file is part of the CardioVascular Wawes Analysis toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// Copyright (C) - INRIA - Alessandro Monti, Claire Médigue, Michel Sorine
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(signal,options)

  err_msg1=_("%s: Wrong type for input argument #%d: A real vector of float expected.\n");
  err_msg2=_("%s: Wrong type for input argument #%d: A struct with fields in {%s} expected.\n")
  //Input signals check
  if type(signal)<>1|~isreal(signal)|and(size(signal)>1) then 
    error(msprintf(err_msg1,"TimeMoments",1))
  end
  signal=matrix(signal,1,-1);
  
  //Default tuning parameters
  twl=77; //time smoothing window length
  fwl=75;  //frequency smoothing window length 
  nf=128;  //number of frequency points
  LPwl=195; //Low pass filter window length 
  LP=wfir("lp",LPwl,[0.01 0],"hm",[0 0]);
  LP=LP/sum(LP);//normalize to get a unit gain at 0 frequency 

  if argn(2)==2 then
    err_msg3=_("%s: Wrong type for input argument #%d: A real positive number expected.\n");
    err_msg4=_("%s: Wrong value for field %s of input argument #%d: An odd number expected.\n")
    err_msg5=_("%s: Wrong value for field %s of input argument #%d: A power of two expected.\n")
    err_msg6=_("%s: Wrong value for input argument #%d: A struct with fields %s expected.\n")

    if typeof(options)=="st" then
      legal_fields=["lowpass","timewindowlength","frequencywindowlength","frequencybins"];
      fn=fieldnames(options)
      for k=1:size(fn,'*')
        select fn(k)
        case "lowpass" then
          //low pass filter
          LP=options.lowpass
          if type(LP)<>1|~isreal(LP)|and(size(LP)>1) then 
            error(msprintf(err_msg2,"TimeMoments",fn(k),2))
          end
          if modulo(size(LP,'*'),2)<>1 then
            error(msprintf(err_msg3,"TimeMoments",fn(k),2))
          end
           LP=LP/sum(LP);//normalize to get a unit gain at 0 frequency 
           LPwl=size(LP,'*')
        case "timewindowlength" then
          //time smoothing window length
          twl=options(fn(k));
          if type(twl)<>1|~isreal(twl)|size(twl,'*')<>1|twl<1 then 
            error(msprintf(err_msg3,"TimeMoments",fn(k),2))
          end
          if modulo(size(twl,'*'),2)<>1 then
            error(msprintf(err_msg4,"TimeMoments",fn(k),2))
          end
        case "frequencywindowlength"  then
          //frequency smoothing window length
          fwl=options(fn(k));
          if type(fwl)<>1|~isreal(fwl)|size(fwl,'*')<>1|fwl<1 then 
            error(msprintf(err_msg3,"TimeMoments",fn(k),2))
          end
          if modulo(size(fwl,'*'),2)<>1 then
            error(msprintf(err_msg4,"TimeMoments",fn(k),2))
          end
        case "frequencybins"  then
          //frequency smoothing window length
          nf=options(fn(k));
          if type(nf)<>1|~isreal(nf)|size(nf,'*')<>1|nf<1 then 
            error(msprintf(err_msg3,"TimeMoments",fn(k),2))
          end
          if nf<>2^round(log2(nf)) then
            error(msprintf(err_msg5,"TimeMoments",fn(k),2))
          end
        else
          error(msprintf(err_msg6,"TimeMoments",2,strcat(legal_fields,",")))
        end
      end
    else
      error(msprintf(err_msg6,"TimeMoments",2,strcat(legal_fields,",")))
    end
  end
  //Compute the signal delay introduced by the filter and the smooting window
  delay=(LPwl-1)/2
  //extend the signal
  signal($+1:$+delay)=signal($);
  //Compute the Smoothed wigner Ville distribution
  G=window('hm',twl);  //time smoothing window.
  H=window('hm',fwl); //frequency smoothing window, in the time-domain,
  if %t
    //Ctfrspwv manages real signals, it is not useful to compute analytic
    //signal
  
    [TFR,T,F]=Ctfrspwv(signal,1:size(signal,'*'),nf,G,H);
    T=T-1;
    //Compute the time moments:
    //power IPow(t)=|signal_z(t)|^2=intg(TFR(t,w)dw. the normalisation by
    //nf is done later
    //energy (time marginal)
    IPow  = 2*sum(TFR,1);//time marginal energy
    //instantaneous frequency (first moment in frequenxy)
    //Ifreq(t)=intg(w*TFR(t,w)dw)/intg(TFR(t,w)dw)/
    IFreq = 4*F(1:nf/2)*TFR(1:nf/2,:)./IPow; 
    //instantaneous dispersion (second moment in frequency)
    IDisp = sqrt(abs((F(1:nf/2).^2)*TFR(1:nf/2)./IPow-IFreq.^2));
  else
    [TFR,T,F]=Ctfrspwv(hilbert(signal),1:size(signal,'*'),nf,G,H);
    T=T-1;
    IPow  = sum(TFR,1);      
    IFreq = F*TFR./IPow;  
    IDisp = sqrt(abs((F.^2)*TFR./IPow-IFreq.^2));
  end
  //Apply low pass filter
  IPow=filter(LP,1,IPow/nf);
  IPow(IPow<0)=0; //Power must be >=0
  IFreq=filter(LP,1,IFreq);
  IDisp= filter(LP,1,IDisp);
  
  //Deduce instantaneous amplitude
  IAmp=sqrt(IPow);
  
endfunction
