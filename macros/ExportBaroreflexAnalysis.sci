// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ExportBaroreflexAnalysis(res,filetype,filename)
  if typeof(res)<>"BRA" then
    error(msprintf(_("%s: Wrong type for argument %d: %s expected.\n"),...
                   "ExportBaroreflexAnalysis",1,"BRA"))
  end
  
  if argn(2)<2 then
    filetype="txt"
  elseif type(filetype)<>10|size(filetype,'*')<>1 then
    error(msprintf(_("%s: Wrong type for argument %d: A String expected.\n"),...
                   "ExportBaroreflexAnalysis",2))
    filetype=stripblanks(filetype)
    if and(filetype<>["txt" "csv"]) then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set  {%s}.\n"),...
                     "ExportBaroreflexAnalysis",2,"""txt"",""csv"""))
    end
  end

  if argn(2)<3 then
    filename=uiputfile("*."+filetype)
    if length(filename)==0 then return,end
  elseif type(filename)<>10|size(filename,'*')<>1 then
    error(msprintf(_("%s: Wrong type for argument %d: A String expected."),...
                   "ExportBaroreflexAnalysis",3))
    filename=stripblanks(filename)
    if length(filename)==0 then
      error(msprintf(_("%s: Wrong value for argument %d: A non empty dtring expected."),...
                     "ExportBaroreflexAnalysis",3))
    end
  end
  
  
  F = fieldnames(res)';
  res.resulttime= res.resulttime'
  F(F=="CoherenceThreshold")=[];
  F(F=="datatime")=[];
  F(F=="RR")=[];
  F(F=="RR_filtered")=[];
  F(F=="SBP")=[];
  F(F=="SBP_filtered")=[];
  
  n=size(res(F(1)),1);
  D=[]
  H=[]
  for k=1:size(F,'*')
    r= res(F(k))
    D=[D r];
    if size(r,2)>1 then
      h=F(k)+" ("+string(1:size(r,2))+")"
    else
      h=F(k)
    end
    H=[H h];
  end
  if filetype=="csv" then
    D=[H;strsubst(string(D),'.',',')];
    write_csv(D, filename,ascii(9))  
  else
    D=[H;string(D)];
    T=emptystr(size(D,1),1);
    for k=1:size(D,2)
      T=T+part(D(:,k),1:max(length(D(:,k)))+2);
    end
    mputl(T,filename);
  end
endfunction


