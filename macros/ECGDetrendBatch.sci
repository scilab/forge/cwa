//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ECGDetrendBatch(filein,fileout,nsec,startindex)
//use sliding mean to remove the baseline variation
  if argn(2)<4 then startindex=1;end
  if argn(2)<3 then nsec=4;end
 
  
  in=mopen(filein,'rb');
  if argn(2)<2 then
    [path, fname, extension] = fileparts(filein)
    fileout=strsubst(filein,extension,"_dt.ecgs")
  else
    if filein==fileout then
      error(msprintf(_("%s: Incompatible input arguments #%d and #%d: input and output filenames must be different\n"),"detrendECGBatch",1,2))
    end
  end

  if ascii(mget(9,'c',in))<>"ECGScilab" then
    mclose(in)
    error(msprintf(_("%s: Wrong value for input argument #%d: Unexpected file type"),"detrendECGBatch",1))
  end
  
  out=mopen(fileout,'wb');
  t0=mget(6,"s",in);
  Nchannels=mget(1,'i',in);
  Nsamples=mget(1,'i',in);
  fe=mget(1,'f',in);
  
  //Detrend window
  nf=nsec*fe;
  if int(nf/2)*2<>nf then nf=nf+1;end
  w=window("hn",nf);w=w/sum(w);

  mput(ascii("ECGScilab"),'c',out)
  mput(t0,'s',out);
  mput(Nchannels,'i',out)
  mput(Nsamples,'i',out)
  mput(fe,'f',out)

  pos=(startindex-1)*Nchannels*8;
  mseek(pos,in,"cur");
  
  BlockLength=10*nf;
 
  winH=mywaitbar("open",_("Processing detrend"));
  //Boucle sur les blocs

  Nb=ceil((Nsamples-startindex+1)/BlockLength);
  e=zeros(Nchannels,nf-1)
  //first data block
  for b=1:Nb
    S=matrix(mget(Nchannels*BlockLength,'d',in),Nchannels,-1)';
    Ns=size(S,1)
    R=[];
    if b==1 then //first data block
      St=[];
      for k=1:Nchannels
        [c,e(k,:)]=convol(w,[ones(nf/2,1)*S(1,k);S(:,k)]);
        c=c(nf+1:$)
        nc=size(c,'*');
        R(:,k)=S(1:nc,k)-c'
      end
      St=S(nc+1:Ns,:);
      nst=size(St,1);
    elseif b==Nb then //last data block
      for k=1:Nchannels
        c=convol(w,[S(:,k);ones(nf/2,1)*S($,k)],e(k,:));
        c=c(1:$-nf+1);
        nc=size(c,'*');
        R(:,k)=[St(:,k);S(1:nc-nst,k)]-c';
      end
      St=S(nc-nst+1:$,:);  
      nst=size(St,1);
    else //intermediate blocks
      for k=1:Nchannels
        [c,e(k,:)]=convol(w,S(:,k),e(k,:));
        nc=size(c,'*');
        R(:,k)=[St(:,k);S(1:nc-nst,k)]-c';
      end
      St=S(nc-nst+1:$,:); 
      nst=size(St,1);
    end
    mput(R',"d",out);
    mywaitbar("update",b/Nb,winH)
  end
  mywaitbar("close",winH);
  mclose(in);
  mclose(out);
  
endfunction
