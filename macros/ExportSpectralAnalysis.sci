// This file is part of the CardioVascular Wave Analysis toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ExportSpectralAnalysis(res,filetype,filename,InputNames)
  if typeof(res)<>"RRA" then
    error(msprintf(_("%s: Wrong type for argument %d: %s expected.\n"),...
                   "ExportSpectralAnalysis",1,"RRA"))
  end
  if argn(2)<4|InputNames==[] then 
    InputNames=["RR","Vt"]
  end
  if size(InputNames,'*')>=2 then InputNames(2)="Vt";end

  if argn(2)<3|filename==[] then
    filename=uiputfile("*."+filetype)
    if length(filename)==0 then return,end
  elseif type(filename)<>10|size(filename,'*')<>1 then
    error(msprintf(_("%s: Wrong type for argument %d: A String expected."),...
                   "ExportSpectralAnalysis",3))
    filename=stripblanks(filename)
    if length(filename)==0 then
      error(msprintf(_("%s: Wrong value for argument %d: A non empty dtring expected."),...
                     "ExportSpectralAnalysis",3))
    end
  end
  
  if argn(2)<2|filetype==[] then
    filetype="txt"
  elseif type(filetype)<>10|size(filetype,'*')<>1 then
    error(msprintf(_("%s: Wrong type for argument %d: A String expected.\n"),...
                   "ExportSpectralAnalysis",2))
    filetype=stripblanks(filetype)
    if and(filetype<>["txt" "csv"]) then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set  {%s}.\n"),...
                     "ExportSpectralAnalysis",2,"""txt"",""csv"""))
    end
  end

  F = fieldnames(res)';
  n=size(res(F(1)),1);
  D=zeros(n,size(F,'*'));
  for k=1:size(F,'*')
    D(:,k)=res(F(k));
  end
  F(2:3)=InputNames(1:2);
  if filetype=="csv" then
    D=[F;strsubst(string(D),'.',',')];
    write_csv(D, filename,ascii(9))  
  else
    D=[F;string(D)];
    T=emptystr(size(D,1),1);
    for k=1:size(F,'*')
      T=T+part(D(:,k),1:max(length(D(:,k)))+2);
    end
    mputl(T,filename);
  end
endfunction


