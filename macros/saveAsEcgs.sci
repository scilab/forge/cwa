//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function saveAsEcgs(S,fs,fout)
//Save an ECG signal into a Scilab ECG (ecgs) binary file
//S : array of the ECG data (one column per channel)
//fe :sampling frequency
//fout : string the .ecgs file name
  if typeof(S)=="sciecg" then
    fout=fs
    fs=S.fs
    S=S.sigs
  else
    if type(S)<>1|~isreal(S) then
      error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n"),...
                     "saveAsEcgs",1))
    end
   
    if type(fs)<>1|~isreal(fs)|size(fs,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),...
                     "saveAsEcgs",2))
    end
    if fs<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),...
                     "saveAsEcgs",2))
    end
  end
  out=mopen(fout,"wb");
  mput(ascii("ECGScilab"),'c',out);
  mput(zeros(1,6),'s',out);//t0;
  mput(size(S,2),'i',out);
  mput(size(S,1),'i',out);
  mput(fs,'f',out);
  mput(matrix(S',-1,1),'d',out);
  mclose(out);
endfunction
