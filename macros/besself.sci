// Copyright (C) 2013 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function S=besself(n, fc, ftype)
//Bessel analog filter design.
// According to ftype S=besself(n,fc,ftype) returns an n'th order analog Bessel
// filter.
  
// if ftype=="lp" a low pass filter is returned, fc is the frequency (Hz)
//    up to which the group delay is approximately constant  
  
// if ftype=="hp" a high pass filter is returned , fc is the frequency (Hz)
//    from which the group delay is approximately constant 
  
// if ftype=="bp" a band pass filter is returned , fc=[fmin max] give the
//    cutoff frequencies (Hz)
  
// if ftype=="sb" a stop band  filter is returned , fc=[fmin max] give the
//    cutoff frequencies (Hz)

// The filter S is returned in a state space form.
  
// The larger the filter order, the better the filter's group delay will
// approximate a constant up to the frequency fc.
//

// References:
//   [1] T. W. Parks and C. S. Burrus, Digital Filter Design,
//       John Wiley & Sons, 1987, chapter 7, section 7.3.3.

  Wn=2*%pi*fc;

  if or(ftype==["bp" "sb"]) then
    Bw = Wn(2)-Wn(1);
    Wn = sqrt(Wn(1)*Wn(2));
  end

  //Get roots of the normalized reversed Bessel polynomial
  //If n is odd the real root appears first
  p = revert_bessel_roots(n);
  
  //Create the filter state space representation from a set of small
  //state space system in series
  if modulo(n,2)==1 then
    S=syslin('c',real(p(1)),1,1,0);
  else
    S=syslin('c',[],[],[],1)
  end
  for k=modulo(n,2)+1:2:n
    pk=p(k);
    m=abs(pk);c=real(pk)/m;s=imag(pk)/m;
    S=S*syslin('c',[c s;-s c]*m,[0;1/(s*m)],[1 0],0);
  end
 
  //Transform to lowpass, bandpass, highpass, or bandstop of desired Wn
  if ftype == "lp" then	
    S.A=Wn*S.A
    S.B=Wn*S.B
  elseif ftype == "bp" then
    [nC,nB,nA]=size(S);
    q=Wn/Bw;
    S.A=Wn*[S.A/q, eye(nA,nA); -eye(nA,nA), zeros(nA,nA)];
    S.B=Wn*[S.B/q;zeros(nA,nB)];
    S.C=[S.C,zeros(nC,nA)];
  elseif ftype == "hp" then
    S.D = S.D - S.C/S.A*S.B;
    S.C = S.C/S.A;
    S.B = -Wn*(S.A\S.B);
    S.A =  Wn*inv(S.A);
  elseif ftype == "sb" then
    [nC,nB,nA]=size(S);
    q = Wn/Bw;
    S.D = S.D - S.C/S.A*S.B;
    S.C=[S.C/S.A,zeros(nC,nA)];
    S.B=-Wn*[(S.A\S.B)/q;zeros(nA,nB)];
    S.A=Wn*[inv(S.A)/q, eye(nA,nA); -eye(nA,nA), zeros(nA,nA)];
  end
endfunction
