function [PSD,F]=calcLomb(t,y,nfft,maxF)
//calLomb - Calculates the PSD using Lomb-Scargle method.
//
//Inputs:
//Outputs:
  
//Calculate PSD
  deltaF=maxF/nfft;
  F = linspace(0.0,maxF-deltaF,nfft);
  PSD=lomb2(y,t,F,%f)*1e6; //calc lomb psd
  // [F,PSD]=Lomb(t,y,4,2,nfft)
  // k=find(F>maxF,1)
  // if k<>[] then
  //   F=F(1:k-1)
  //   PSD=PSD(1:k-1)
  // end
endfunction
