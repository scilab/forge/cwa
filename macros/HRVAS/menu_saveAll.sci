function menu_saveAll()
// Export HRV
  c=gcbo();while c.type<>"Figure" then c=c.parent;end
  ud=c.user_data;
 
  outfile=uiputfile('*',pwd(),'Save HRV As');
  if outfile<>"" then
    magic="HRV_"+strcat(string(HRVAS_version()),'.')
    HRV=ud.HRV
    IBI=ud.IBI
    settings=ud.settings
    art=ud.art
    trend=ud.trend
    nIBI=ud.nIBI
    dIBI=ud.dIBI
    save(outfile,'magic','HRV','IBI','nIBI','dIBI','art','trend','settings')
  end    
endfunction
