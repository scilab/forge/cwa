function updateNLTbl(h,hrv,opt)
    // updateNLTbl: update Nonlinear results table with hrv data    
        hrv=hrv.nl;   //nonlinear hrv
        tH=h.text.nl; //handles of text objects
        set(tH(2,3),'text',msprintf('%0.3f',hrv.sampen($)))
        set(tH(4,3),'text',sci2exp(hrv.dfa.alpha))
        set(tH(5,3),'text',sci2exp(hrv.dfa.alpha1))
        set(tH(6,3),'text',sci2exp(hrv.dfa.alpha2))
endfunction
