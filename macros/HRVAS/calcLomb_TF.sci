function [PSD,F,T]=calcLomb_TF(t,y,nfft,maxF,winSize,overlap)
//calLomb - Calculates PSD using windowed Lomb-Scargle method.
//
//Inputs:
//Outputs:            
  
//get limits of windows
  if t($)>=winSize
    idx=slidingWindow(t,winSize,overlap,1);
  else
    idx=[1 length(t)];
  end
  T=t(idx(:,1))+round(winSize/2); //estimate the center of the windows for plotting                
  
  //preallocate memory
  nPSD=size(idx,1); //number of PSD/windows
  PSD=zeros(nfft,nPSD);
  t2=zeros(nPSD,1);
  
  deltaF=maxF/nfft;        
  F = linspace(0.0,maxF-deltaF,nfft)';
  for i=1:nPSD
    //Prepare y2 and t2
    y2=y(idx(i,1):idx(i,2));
    t2=t(idx(i,1):idx(i,2));
    
    //remove linear trend
    y2=detrend(y2,'linear');
    y2=y2-mean(y2); //remove mean
    PSD(:,i)=lomb2(y2,t2,F,%f)'; 
  end
endfunction
