// Copyright (C) 2010, John T. Ramshur, jramshur@gmail.com
// 
// This file is part of HRVAS
//
// HRVAS is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// HRVAS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
function output = nonlinearHRV(ibi,m,r,nRange,breakpoint)
//nonlinearHRV(IBI,opt) - calculates nonlinear HRV
//
//Inputs:    IBI = inter-beat interval (s) and time locations (s)
//           opt = analysis options from gui
//           
//Outputs:   output is a structure containg all HRV.
 
  output.sampen=SampEn(ibi(:,2),m,r)
  //output.sampen=sampen(ibi(:,2),m,r,0,0);
  output.dfa=DFA(ibi(:,2),nRange(1):nRange(2),breakpoint);
endfunction

