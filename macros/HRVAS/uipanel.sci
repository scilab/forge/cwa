function h=uipanel(varargin)
  border = createBorder("line", "white",1)
  for k=1:2:size(varargin)
    if convstr(varargin(k))=='title' then
      fnt=createBorderFont("",0,"bold")
      border = createBorder("titled",border, varargin(k+1),"","",fnt)
      varargin(k+1)=null()
      varargin(k)=null()
      break
    end
  end
  h=uicontrol("Style","frame","relief","groove","border",border,varargin(:))

endfunction

