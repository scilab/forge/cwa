function [PSD,F]=calcAR(t,y,fs,nfft,AR_order)
//calAR - Calculates the PSD using Auto Regression model.
//
//Inputs:
//Outputs:
  
//Prepare y    
  t2 = (t(1):1/fs:t($))'; //time values for interp.
  y=interp(t2,t,y,splin(t,y))'; //cubic spline interpolation
  y=y-mean(y); //remove mean
  y = y.*window("hm",length(y)); //hamming window
  
  [PSD,F]=pBurg(y,AR_order,(nfft*2)-1,fs,'onesided');

endfunction

