function output=emptyData(nfft,maxF)
//create output structure of zeros
  
  output.hrv.aVLF=0;
  output.hrv.aLF=0;
  output.hrv.aHF=0;
  output.hrv.aTotal=0;
  output.hrv.pVLF=0;
  output.hrv.pLF=0;
  output.hrv.pHF=0;
  output.hrv.nLF=0;
  output.hrv.nHF=0;
  output.hrv.LFHF=0;
  output.hrv.peakVLF=0;
  output.hrv.peakLF=0;
  output.hrv.peakHF=0;
  
  //PSD with all zeros
  deltaF=maxF/nfft;    
  output.f = linspace(0.0,maxF-deltaF,nfft);
  output.psd=zeros(length(output.f),1);

endfunction

