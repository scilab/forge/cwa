
function tH = createTimeTbl(aH)
// createTimeTbl: creates the table to display time-domain HRV results 
  aH.data_bounds=[0 0;1 1];
  H=0.07
  x=[0.05 0.45 0.8]//relative x position of cols
  y=(1-3*H)-(1:11)*H      

  //Horizontal Lines
  xsegs([0 1],[1 1]*(1-1.25*H))
  xsegs([0 1],[1 1]*(1-2.5*H))
      
  xstring(0.4,1-H*1.25,'Time Domain');e=gce();
  //Col Headers
  hn=['Variables','Units','Values'];
  for l=1:size(hn,'*')
    xstring(x(l),1-2.75*H,hn(l))
  end
  //Column 1
  tH=gce();
  vn=['MeanIBI' 'SDNN' 'MeanHR' 'SDHR' 'RMSSD' 'NNx' 'pNNx' 'SDNNi' ...
     'Geometric measures','HRV Triangular Index' 'TINN'];
  u=['ms' 'ms' 'bpm' 'bpm' 'ms' 'count' '%' 'ms' '' ''  'ms']
  v=['0.0' '0.0' '0.0' '0.0' '0.0' '0' '0.0' '0.0' '' '0.0'  '0.0']
  for l=1:size(vn,'*')
    xstring(x(1),y(l),vn(l));e=gce();e.font_size=0.8;tH(l,1)=e;
    xstring(x(2),y(l),u(l));e=gce();e.font_size=0.8;e.alignment='right';tH(l,2)=e;
    xstring(x(3),y(l),v(l));e=gce();e.font_size=0.8;e.alignment='right';tH(l,3)=e;
  end

endfunction
