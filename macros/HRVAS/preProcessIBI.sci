// Copyright (C) 2010, John T. Ramshur, jramshur@gmail.com
// 
// This file is part of HRVAS
//
// HRVAS is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// HRVAS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

function [dibi,nibi,trend,art]=preProcessIBI(ibi, varargin)
//preProcessIBI: detects ectopic IBI, corrects ectopic IBI, and then
//detrends IBI
//nibi : [t,y],  ectopic IBI corrected
//dibi : [t,y],  detrended nibi
//
// TODO: build function description and usage
 
// PARSE INPUTS
  opt.locateMethod=[];
  opt.locateInput=[];
  opt.replaceMethod="None";
  opt.detrendMethod="none";
  opt.smoothSgolayLen=31;
  opt.smoothSgolayDegree=2;
  
  opt.smoothLoessAlpha=20;//(%)
  opt.smoothLoessOrder=2;
  opt.polyOrder=1;
  opt.waveletType="db3";
  opt.waveletLevels=6;
  opt.fc=0.01;
  opt.resampleRate=4;
  opt.meanCorrection=%f;

  for k=1:2:size(varargin)
    select varargin(k)
    case "locateMethod"
      opt.locateMethod=varargin(k+1)
    case "locateInput"
      opt.locateInput=varargin(k+1)
    case "replaceMethod"
      opt.replaceMethod=varargin(k+1)
    case "detrendMethod"
      opt.detrendMethod=varargin(k+1)
    case "smoothSgolayLen"
      opt.smoothSgolayLen=varargin(k+1)
    case "smoothSgolayDegree"
      opt.smoothSgolayDegree=varargin(k+1)
    case "smoothLoessAlpha"
      opt.smoothLoessAlpha=varargin(k+1)
    case "smoothLoessOrder"
      opt.smoothLoessOrder=varargin(k+1)
    case "polyOrder"
      opt.polyOrder=varargin(k+1)
    case "waveletType"
      opt.waveletType=varargin(k+1)
    case "waveletLevels"
      opt.waveletLevels=varargin(k+1)
    case "fc"
      opt.fc=varargin(k+1)
    case "resampleRate"
      opt.resampleRate=varargin(k+1)
    case "meanCorrection"
      opt.meanCorrection=varargin(k+1)
    end      
  end
  // correct ectopic ibi
  [nibi,art]=correctEctopic(ibi,opt);
  
  // Detrending
  [dibi,trend]=detrendIBI(nibi,opt);
endfunction

function [nibi,art]=correctEctopic(ibi,opt)
    y=ibi(:,2);
    t=ibi(:,1);
    //locate ectopic
    i=find(convstr(opt.locateMethod)=="percent");
    if i<>[] then
        artPer=locateRROutliers(y,"percent",opt.locateInput(i));
    else
        artPer=%f(ones(y));
    end
     //locate ectopic
    i=find(convstr(opt.locateMethod)=="sd");
    if i<>[] then
        artSD=locateRROutliers(y,"sd",opt.locateInput(i));
    else
        artSD=%f(ones(y));
    end
    i=find(convstr(opt.locateMethod)=="median");
    if i<>[] then
        artMed=locateRROutliers(y,"median",opt.locateInput(i));
    else
        artMed=%f(ones(y));
    end
    art=artPer | artSD | artMed; //combine all logical arrays    
    
    //replace ectopic
     select convstr(opt.replaceMethod)
        case "mean"
            [t,y]=replaceRROutliers(t,y,art,"mean",opt.replaceInput);
        case "median"
            [t,y]=replaceRROutliers(t,y,art,"median",opt.replaceInput);
        case "spline"
            [t,y]=replaceRROutliers(t,y,art,"cubic");
        case "remove"
            [t,y]=replaceRROutliers(t,y,art,"remove");            
        else //none
            //do nothing
     end
     nibi=[t,y];
endfunction

function [dibi,trend]=detrendIBI(ibi,opt)
    y=ibi(:,2);
    t=ibi(:,1);
    // preallocate memory and create default trend of 0.
   
    trend=zeros(length(y),2);
    trend(:,1)=t; //time values    
    
    meanIbi=mean(y);
    meth=convstr(opt.detrendMethod)
    if meth=="wavelet" then
      wtype=opt.waveletType(1)+opt.waveletType(2)

      trend(:,2)=DetrendRR(y,"wavelet",opt.waveletLevels,wtype)
    elseif meth=="loess" then
      span=opt.smoothLoessAlpha/100; //convert to decimal percent
      trend(:,2)=DetrendRR(y,"loess",span,opt.smoothLoessOrder);
    elseif meth=="stavisky golay" then
      trend(:,2)=DetrendRR(y,"sgolay", opt.smoothSgolayLen,opt.smoothSgolayDegree);
    elseif meth=="polynomial" then 
      trend(:,2) = DetrendRR(y,"polynomial",opt.polyOrder)
    elseif or(meth==["priors","smoothness priors"]) then
      trend(:,2) = DetrendRR(y,"SPA",opt.fc)
    end
    dibi=[t,y-trend(:,2)]; //detrended IBI

    // Note: After removing the trend, the mean value of the ibi
    // series is near zero and some of the artifact detection methods 
    // detect two many outlers due to a mean near zero
    // Solution: shift detrended ibi back up to it's orignal mean
    // by adding orignal mean to detrended ibi.                        
    if opt.meanCorrection
      dibi(:,2) = dibi(:,2) + meanIbi; // see note above
    end  
endfunction
