function output=calcAreas(F,PSD,VLF,LF,HF,flagNorm)
//calcAreas - Calulates areas/energy under the PSD curve within the freq
//bands defined by VLF, LF, and HF. Returns areas/energies as ms^2,
//percentage, and normalized units. Also returns LF/HF ratio.
//
//Inputs:
//   PSD: PSD vector
//   F: Freq vector
//   VLF, LF, HF: array containing VLF, LF, and HF freq limits
//   flagNormalize: option to normalize PSD to max(PSD)
//Output:
//
//Usage:
//   
//
//   Modified from Gary Clifford's ECG Toolbox: calc_lfhf.m   

  if argn(2)<6 then flagNorm=%f;end
  if or(size(PSD)==1) then PSD=PSD(:),end
  nPSD=size(PSD,2);
  //normalize PSD if needed
  if flagNorm
    f=max(PSD,"r")
    for p=1:nPSD, PSD(:,p)=PSD(:,p)/f(p);end
  end

  // find the indexes corresponding to the VLF, LF, and HF bands
  iVLF= find((F>=VLF(1)) & (F<=VLF(2)));
  iLF = find((F>=LF(1)) & (F<=LF(2)));
  iHF = find((F>=HF(1)) & (F<=HF(2)));
  
  peakVLF=zeros(1,nPSD);
  peakLF=zeros(1,nPSD);
  peakHF=zeros(1,nPSD);
  aVLF=zeros(1,nPSD);
  aLF=zeros(1,nPSD);
  aHF=zeros(1,nPSD);
  for p=1:nPSD //loop on time
    
    //Find peaks
    //VLF Peak

    [pks,ipks] = zipeaks(PSD(iVLF,p));
    if ~isempty(pks)
      [tmpMax,i]=max(pks);        
      peakVLF(p)=F(iVLF(ipks(i)));
    else
      [tmpMax,i]=max(PSD(iVLF,p));
      peakVLF(p)=F(iVLF(i));
    end
    
    //LF Peak
    [pks,ipks] = zipeaks(PSD(iLF,p));
    if ~isempty(pks)
      [tmpMax,i]=max(pks);
      peakLF(p)=F(iLF(ipks(i)));
    else
      [tmpMax,i]=max(PSD(iLF,p));
      peakLF(p)=F(iLF(i));
    end
    
    //HF Peak
    [pks,ipks] = zipeaks(PSD(iHF,p));
    if ~isempty(pks)
      [tmpMax,i]=max(pks);        
      peakHF(p)=F(iHF(ipks(i)));
    else
      [tmpMax,i]=max(PSD(iHF,p));
      peakHF(p)=F(iHF(i));
    end 
    
    // calculate raw areas (power under curve), within the freq bands (ms^2)
    aVLF(p)=inttrap(F(iVLF),PSD(iVLF,p));
    aLF(p)=inttrap(F(iLF),PSD(iLF,p));
    aHF(p)=inttrap(F(iHF),PSD(iHF,p));
  end
  aTotal=aVLF+aLF+aHF;

  //create output structure
  output= struct('aVLF',aVLF, 'aLF',aLF, 'aHF',aHF, 'aTotal',aTotal, ...
                 'pVLF',(aVLF./aTotal)*100,...// areas relative to the total area (%)
                 'pLF',(aLF./aTotal)*100,...
                 'pHF',(aHF./aTotal)*100,...
                 'nLF',aLF./(aLF+aHF), ...//normalized areas (relative to HF+LF, n.u.)
                 'nHF',aHF./(aLF+aHF),...
                 'LFHF',aLF./aHF, ...
                 'peakVLF',peakVLF,'peakLF',peakLF, 'peakHF',peakHF);
  

endfunction
