function plotPoincare(h,ibi,hrv,opt)
//// Helper: Plot - Poincare
  hrv=hrv.time;
  sca(h.axesPoincare);delete(h.axesPoincare.children);
  ibi=ibi(:,2);
  PoincareHRVPlot(ibi,"ellipse")
  m1=mean(ibi(1:$-1));
  m2=mean(ibi(2:$));
  sd=0.5*stdev(diff(ibi))^2
  SD1=sqrt(sd)*1e3;
  SD2=sqrt(2*stdev(ibi)^2-sd)*1e3;
  
  title(msprintf("y=-x+%.3g, SD1=%.1f ms, SD2=%.1f ms",m1+m2,SD1,SD2))
endfunction
