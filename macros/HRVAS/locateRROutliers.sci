// Copyright (C) 2010, John T. Ramshur, jramshur@gmail.com
// 
// This file is part of HRVAS
//
// HRVAS is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// HRVAS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
function outliers = locateRROutliers(s, method, opt1, opt2)
//locateOutliers: locates artifacts/outliers from data series
//
//  Inputs:  s = array containg data series
//           method = artifact removal method to use.
//  methods: 'percent' = percentage filter: locates data > x percent diff than previous data point.               
//           'sd' = standard deviation filter: locates data > x stdev away from mean.
//           'above' = Threshold filter: locates data > threshold value
//           'below' = Threshold filter: locates data < threshold value
//           'median' = median filter. Outliers are located.
//Outputs:   outliers = logical array of whether s is artifact/outlier or not
// 
// Reference:
// Thuraisingham, R. A. (2006). "Preprocessing RR interval time series
// for heart rate variability analysis and estimates of standard 
// deviation of RR intervals." Comput.Methods Programs Biomed.  
//Examples:
//   Locate outliers with 20// percentage filter:
//       outliers = locateOutlers(s,'percent',0.2)
//   Locate outliers that are above a threshold of 0.5:
//       outliers = locateOutlers(s,'thresh','above',0.5)
//   Locate outliers with median filter:
//       outliers = locateOutlers(s,'median',4,5)
//
    //check inputs
    if argn(2) < 3 then
      error('Not enough input arguments')
      return;
    end
    if and(size(s)>1) then  error('Input array must be 1-dim');end
    s=s(:);

    select convstr(method)
    case 'percent' //percentage filter
      outliers = percentFilter(s,opt1);
    case 'sd' //sd filter
      outliers = sdFilter(s,opt1);
    case 'thresh' //threshold filter
      outliers = threshFilter(s,opt1);
    case 'median' //median filter
      [outliers] = medianFilter(s,opt1);
    else
      outliers=%f(ones(s));
    end
endfunction
    


