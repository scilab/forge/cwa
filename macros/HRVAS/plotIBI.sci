function plotIBI(h,opt,IBI,dIBI,nIBI,trend,art)        
// plotIBI: plots ibi, trendline, and outliers/artifacts in GUI
// 
// Inputs:
//   h: handle used for plotting
//   opt: structure containing hrv analysis options
//   IBI, dIBI, nIBI: original, detrended + ectopic correcton
//
  cla(h.axesIBI);sca(h.axesIBI)
  showStatus('Plotting IBI');
  trend=trend;
  //Plot IBI Data       
  if opt.ArtReplace=='none' then //highlight Artifacts
    t=IBI(:,1);
    y=IBI(:,2);
    linecolor='y';
  else //plot preprocessed ibi            
    if opt.ArtReplace<>'remove' then
      t=nIBI(:,1);
      y=nIBI(:,2);
      linecolor='r';
    else //if removing artifacts plot original
      t=IBI(:,1);
      y=IBI(:,2);
      linecolor='r';
    end
  end
  drawlater()
  //plot IBI
  h.axesIBI.auto_clear="on";
  h.axesIBI.auto_ticks(1)='on';
  plot(h.axesIBI,t,y,'.-')

  h.axesIBI.auto_clear="off";
  e=gce();e.children.mark_size_unit="point",e.children.mark_size=4;
  if or(art) then
    plot(h.axesIBI,t(art),y(art),'.r')
  end
  //plot trend

  if opt.Detrend<>'none' then
    plot(h.axesIBI,trend(:,1),trend(:,2)+mean(y),'r','LineWidth',2);
  end
  
  //determine y axes limits
  yrange=abs(max(y)-min(y));
  ylim=[min(y)-(yrange*0.05), max(y)+(yrange*0.05)];                        
  //determine x axes limits and labels
  xlim=[min(t) max(t)];
  //determine xtick labels
  
  //set tick lables and axes limits
  xlabel(h.axesIBI,'Time (hh:mm:ss)');
  ylabel(h.axesIBI,'IBI (s)');
  h.axesIBI.data_bounds=[xlim' ylim'];
  xtick=h.axesIBI.x_ticks;
  for i=1:length(xtick.locations)
    v=xtick.locations(i)
    HH=floor(v/3600);v=v-HH*3600;MM=floor(v/60);SS=v-MM*60;
    xtick.labels(i) = msprintf("%.2d:%.2d:%.2d",HH,MM,SS);
  end
  h.axesIBI.auto_ticks(1)='off';
  h.axesIBI.x_ticks=xtick
  drawnow()

endfunction   
