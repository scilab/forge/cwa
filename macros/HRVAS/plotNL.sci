function plotNL(h,hrv,opt)
  hrv=hrv.nl;   //nonlinear hrv      
  
  //plot DFA
  x=log10(hrv.dfa.n); y=log10(hrv.dfa.F_n);
  x=x(:);y=y(:)
  h.axesNL.auto_clear="on";
  h.axesNL.tight_limits="on";
  plot(h.axesNL,x,y,'.','MarkerSize',3)                
  ibreak=find(hrv.dfa.n==opt.breakpoint);
  //short term fit
  lfit_a1=horner(hrv.dfa.alpha1,x(1:ibreak));
  //long term fit
  lfit_a2=horner(hrv.dfa.alpha2,x(ibreak+1:$));
  h.axesNL.auto_clear="off"
  plot(h.axesNL,x(1:ibreak),lfit_a1(:),'r-', 'linewidth',2)
  plot(h.axesNL,x(ibreak+1:$),lfit_a2(:),'g-','linewidth',2)
  
  xrange=abs(max(x)-min(x)); xadd=xrange*0.06;
  xlim=[min(x)-xadd, max(x)+xadd];
  yrange=abs(max(y)-min(y)); yadd=yrange*0.06;
  ylim=[min(y)-yadd, max(y)+yadd];
  set(h.axesNL,"data_bounds",[xlim(:),ylim(:)])
  title(h.axesNL,'DFA')
  xlabel(h.axesNL,'$\log_{10} n$')
  ylabel(h.axesNL,'$\log_{10} F(n)$')
endfunction
