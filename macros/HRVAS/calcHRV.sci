function output=calcHRV(F,PSD,VLF,LF,HF)
// calcAreas - Calulates areas/energy under the PSD curve within the freq
// bands defined by VLF, LF, and HF. Returns areas/energies as ms^2,
// percentage, and normalized units. Also returns LF/HF ratio.
//
// Inputs:
//   PSD: 2D array, each column contines PPSD for a given time range
//   F: Freq vector
//   VLF, LF, HF: array containing VLF, LF, and HF freq limits
// Output:
//
// Usage:
//   
//
// Ref: Modified from Gary Clifford's ECG Toolbox: calc_lfhf.m   
    
  output=calcAreas(F,PSD,VLF,LF,HF);
  output.rLFHF=sum(output.LFHF>1)/sum(output.LFHF<=1);
endfunction

