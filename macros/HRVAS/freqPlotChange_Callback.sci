function freqPlotChange_Callback(m)
// Callback function run when freq plot type change
  c=gcbo();while c.type<>"Figure" then c=c.parent;end
  ud=c.user_data;
  h=ud.h
  HRV=ud.HRV
  settings=ud.settings
  if HRV<>[] then
    if  m=='Welch' then
      psd=HRV.freq.welch.psd;
      f=HRV.freq.welch.f;
      ylbl='PSD (s^2/Hz)';
      flagLS=%f;
    elseif m=='Burg' then
      psd=HRV.freq.ar.psd;
      f=HRV.freq.ar.f;
      ylbl='PSD (s^2/Hz)';
      flagLS=%f;
    else
      psd=HRV.freq.lomb.psd;
      f=HRV.freq.lomb.f;
      ylbl='PSD (normalized)';
      flagLS=%t;
    end
    plotPSD(h.axesFreq,f,psd,settings.VLF,settings.LF, ...
            settings.HF,[],[],%t,flagLS);
    xlabel(h.axesFreq,'Freq (Hz)');
    ylabel(h.axesFreq,ylbl);            
  end
endfunction
