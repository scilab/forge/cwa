function btnChooseFile_Callback()
  c=gcbo();ud=c.parent.parent.user_data;
  settings=ud.settings
  h=ud.h
// Callback function executed when btnChooseFile is pressed

  filename="";
  if settings<>[] then filename=settings.file;end
  filename = uigetfile( ...
      ['*.ibi;*.rr;*.txt','IBI Files (*.ibi,*.rr,*.txt)';
       '*.ibi',  'IBI (*.ibi)'; ...
       '*.rr','RR (*.rr)'; ...
       '*.txt','Text (*.txt)'; ...
       '*.*',  'All Files (*.*)'], ...
      'Select IBI data file',...
      filename);
        
  if filename=="" then
    return //user selected cancel
  else
    filename=stripblanks(filename)
    k=find(filename=="");if k<>[] then filename(k)=[];end
    set(h.txtFile,'String',filename);
  end

endfunction
