function [PSD,F,T]=calcAR_TF(t,y,fs,nfft,AR_order,winSize,overlap)
//calAR - Calculates PSD using windowed Burg method.
//
//Inputs:
//Outputs:          

    winSize=winSize*fs; // (samples)
    overlap=overlap*fs; // (samples)
    
    //resample
    tint = t(1):1/fs:t($); //time values for interp.
    y=interp(tint,t,y,splin(t,y))'; //cubic spline interpolation            
        
    //get limits of windows    
    if tint($)>=winSize then
        idx=slidingWindow(tint,winSize,overlap,0); //(sample #)
    else
        idx=[1 length(tint)];
    end
    T=tint(idx(:,1)+round(winSize/2)); //calculate center time of window (s)
                                       //used for plotting    
    
    //preallocate memory
    nPSD=size(idx,1); //number of PSD/windows
    PSD=zeros(nfft,nPSD);    
        
    //Calculate PSD
     for i=1:nPSD
        //Prepare y2 and t2
        y2=y(idx(i,1):idx(i,2));
        t2=tint(idx(i,1):idx(i,2));        
        
        y2=y2-mean(y2); //remove mean
        y2 = y2.*window("hm",length(y2))'; //hamming window

        //Calculate PSD                 
        [psd,f]=pBurg(y2,AR_order,(nfft*2)-1,fs,'onesided');        
        PSD(:,i)=psd(:);
     end
     F=f(:);
     T=T(:);
endfunction
