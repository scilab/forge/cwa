function menu_loadAll()
// Export HRV
  c=gcbo();while c.type<>"Figure" then c=c.parent;end
  ud=c.user_data;
  h=ud.h
  infile=uigetfile('*',pwd(),'Select file to load');
  if infile<>"" then
    load(infile)
    if exists("magic","local")==1&magic=="HRV_"+strcat(string(HRVAS_version()),'.') then
      ud.settings=settings
      setSettings(ud.settings)
      ud.HRV=HRV
      ud.IBI=IBI
      ud.nIBI=nIBI
      ud.dIBI=dIBI
      ud.art=art
      ud.trend=trend
      set(c,"user_data",ud)
      plotIBI(h,ud.settings,ud.IBI,ud.dIBI,ud.nIBI,ud.trend,ud.art)  
      displayHRV(h,ud.IBI,ud.dIBI,ud.HRV,ud.settings)  
    end
  end    
endfunction
