function btnRun_Callback(c)
  if argn(2)<1 then
    c=gcbo();while c.type<>"Figure" then c=c.parent;end
  end
  ud=c.user_data;
  h=ud.h
  st=h.lblStatus;
  st.children.icon=""
  //f=get(h.txtFile,'String');
  if ud.IBI<>[] then

    [settings,ok]=getSettings(); //get HRV options from gui
    if ~ok then return,end
    ieee_s=ieee();ieee(2);//not to trap zero divide
    try
      [HRV,dIBI,nIBI,trend,art]=getHRV(h,ud.IBI,settings);
      displayHRV(h,nIBI,dIBI,HRV,settings);
      
      ud.settings=settings;
      ud.HRV=HRV;
      ud.dIBI=dIBI;
      ud.nIBI=nIBI;
      ud.art=art;
      ud.trend=trend;
      set(c,"user_data",ud);
    catch
      st=h.lblStatus
      m=st.children.string
      st.children.string=strsubst(m,"Please wait","An error occured")
      st.children.icon=pathconvert(SCI+"/scilab/modules/gui/images/icons/16x16/status/error.png",%f)
    end
    h.menuSave.enable='on';
    h.menuExport.enable='on';
    ieee(ieee_s);
  end
endfunction
