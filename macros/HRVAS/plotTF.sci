function plotTF(h,hrv,opt)
//// Helper: Plot - TF
  showStatus('Plotting Time Frequency');
  k=find(h.btngrpTFPlot.children.Tag=="TFMeth");
  listTFMeth=h.btngrpTFPlot.children(k);
  m= listTFMeth.string(listTFMeth.value)
  h.axesTF.auto_ticks="on";
  h.axesTF.axes_reverse="off";

  if  m=='Burg' then
    key="ar"
  elseif m=='LombScargle' then
    key="lomb"
  elseif m=="Wavelet" then   
    key="wav"
  elseif m=="SPWV" then   
    key="spwv"
  end
  flagWavelet= key=="wav";
  
  f=hrv.tf(key).f;
  t=hrv.tf(key).t;
  if t==[] then showStatus('');return;end
  lf=hrv.tf(key).hrv.aLF;
  hf=hrv.tf(key).hrv.aHF;   
  lfhf=hrv.tf(key).hrv.LFHF;
  
  if key<>"wav" then
     flagVLF=%f//do not plot vlf in global PSD   
    //interpolate lf/hf time series for a smoother plot
    t2 = linspace(t(1),t($),100); 
    if size( hrv.tf(key).psd,2)>1
      lfhf=interp1(t,lfhf,t2,"spline")'; //interpolation
    end
  else
    t2=t
  end
  // temp: only plot from 0-0.6 Hz
  freqLim=1.1*opt.HF($);
  fi=(f<=freqLim);
  
  //Type of plot (spectrogram, global PSD, etc.)
  pt=get(h.listTFPlot,'string');
  pt=pt(get(h.listTFPlot,'value'));
  cla(h.axesTF)
  select convstr(pt)
  case 'spectrogram'
    h.axesTFCB.visible="on";
    h.axesTF.axes_bounds(3)=0.73;
    plotSpectrogram(h.axesTF,t, f(fi), hrv.tf(key).psd(fi,:),  ...
                    opt.VLF,opt.LF,opt.HF,[],[],flagWavelet);
    xlabel(h.axesTF,'Time (s)');
    ylabel(h.axesTF,'Freq (Hz)'); 
  case 'spectrogram (log)'
    h.axesTFCB.visible="on";
    h.axesTF.axes_bounds(3)=0.73;
    plotSpectrogram(h.axesTF,t, f(fi), log(hrv.tf(key).psd(fi,:)),...
                      opt.VLF, opt.LF,opt.HF,[],[],flagWavelet);
    xlabel(h.axesTF,'Time (s)');
    ylabel(h.axesTF,'Freq (Hz)');                                 
  case 'surface'
    disp([size(t);size(f(fi));size( hrv.tf(key).psd(fi,:))])
    h.axesTFCB.visible="off";
    h.axesTF.axes_bounds(3)=0.8;
    plotWaterfall(h.axesTF,t, f(fi), hrv.tf(key).psd(fi,:),...
                  opt.VLF, opt.LF,opt.HF,'surf',flagWavelet)
    xlabel(h.axesTF,'Time (s)');
    ylabel(h.axesTF,'Freq (Hz)');
    zlabel(h.axesTF,'PSD (s^2/Hz)')
    h.axesTF.z_label.font_angle=-90;
    h.axesTF.x_label.font_angle=-45;
  case 'global psd'  
    h.axesTFCB.visible="off";
    h.axesTF.axes_bounds(3)=0.8;
    plotPSD(h.axesTF,f(fi),hrv.tf(key).global.psd(fi)'*1e6,opt.VLF, ...
            opt.LF,opt.HF,[],[],~flagWavelet);                    
    xlabel(h.axesTF,'Freq (Hz)');
    ylabel(h.axesTF,'Global PSD (s^2/Hz)');
  case 'lf & hf power'
    h.axesTFCB.visible="off";
    h.axesTF.axes_bounds(3)=0.8;
    h.axesTF.auto_clear="on";
    sca(h.axesTF)
    plot(h.axesTF,t,lf*1e6,'r',t,hf*1e6,'b');
    xlabel(h.axesTF,'Time (s)');
    ylabel(h.axesTF,'Power (ms^2)');
    legend(['LF','HF'])
  case 'lf/hf ratio'
    h.axesTFCB.visible="off";
    h.axesTF.axes_bounds(3)=0.8;
    sca(h.axesTF)
    above=((lfhf>1).*lfhf);
    above(above==0)=1;
    below=((lfhf<1).*lfhf);
    below(below==0)=1;   
    h.axesTF.auto_clear="on";

    area(t2(:),above(:),1,'facecolor',[0 1 1])
    h.axesTF.auto_clear="off";
    area(t2(:),below(:),1,'facecolor',[1 0 1])
    xlabel(h.axesTF,'Time (s)');
    ylabel(h.axesTF,'LF/HF (ratio)');
  end
  showStatus('');
endfunction
