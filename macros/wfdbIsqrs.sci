function ann=wfdbIsqrs(ann)
  //search annotation corresponding to R peaks locations 
//derived from http://www.physionet.org/physiotools/matlab/wfdb-subset/old/isqrs.m
  K=['N' 'A' 'V' 'L' 'R' 'J' 'F' 'S' 'j' 'e' 'a' 'r' ...
                    '/' 'E' 'f' 'Q'  '?' 'B' 'n' 'P']
  if type(ann)==10 then
    annotator= fileparts(ann,"extension");
    annotator=part(annotator,2:length(annotator))
    recordname=part(ann,1:length(ann)-length(annotator)-1);
    ann=wfdbReadAnnotations(recordname,annotator);
  end
    k=strindex(strcat(wfdbAnnot2String(ann(2,:))),K);
    ann=ann(:,k);
endfunction
