// This file is part of the CardioVascular  wave analysis toolbox
// Copyright (C)  - INRIA - Alessandro Monti
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function  PlotBaroreflexAnalysis(res,Title,selection)
  if typeof(res)<>"BRA" then
    error(msprintf(_("%s: Wrong type for argument %d: %s expected.\n"),...
                   "PlotBaroreflexAnalysis",1,"BRA"))
  end
  if argn(2)<3 then
    selection=["raw","energy","freq"];
  else
    admissible=["raw","filt","energy","dsp","freq","disp"];
    for k=1:size(selection,'*')
      if and(selection(k)<>admissible) then
        s=strsubst(strsubst(sci2exp(admissible,0),"[",""),"]","");
        error(msprintf(_("%s: Wrong value for input argument #%d: Elements must be in the set {%s}.\n"),...
                       "PlotBaroreflexAnalysis",3,s))
      end
    end
  end

  nplot=2+size(selection,'*');
  kplot=1;

  clf;fig=gcf();
  fig.figure_size = [1200,800];
  idraw=fig.immediate_drawing;
  g=addcolor([1 1 1]*0.8);
  b=color('blue');
  sda();
  da=gda();
  da.grid(1:2)=[g g];
  da.tight_limits="on";
  da.margins(4)=0.07;

  o=0.05; //reserve space for title
  h=(1-2*o)/nplot;
  
  t=res.datatime;
  tr=res.resulttime
  tbounds=[t(1);t($)];
  
  first=%t;
  Ax=[]
  fig.immediate_drawing = "off";

  drawlater()
  if or(selection=="raw") then
    al=CreateAxesL();
    plot(t,res.RR)
    ylabel(_("Raw signals"));
    ar=CreateAxesR();
    plot(t,res.SBP)
    kplot=kplot+1;
    Ax=[Ax;al,ar]
  end
  
  if or(selection=="filt") then
    al=CreateAxesL();
    plot(t,res.RR_filtered)
    ylabel(_("Filtered signals"));
    ar=CreateAxesR();
    plot(t,res.SBP_filtered)
    kplot=kplot+1;
    Ax=[Ax;al,ar]
  end
  
  if or(selection=="energy") then
    al=CreateAxesL();
    BRA_plot2(tr,res.RR_energy,6);
    ylabel(_("Energy"));
    ar=CreateAxesR();
    BRA_plot2(tr,res.SBP_energy,6);
    kplot=kplot+1;
    Ax=[Ax;al,ar]
  end
  
  if or(selection=="freq") then
    al=CreateAxesL();
    BRA_plot2(tr,res.RR_fmin);
    ylabel(_("IFreq (Hz)"));
    ar=CreateAxesR();
    BRA_plot2(tr,res.SBP_fmin);
    kplot=kplot+1;
    Ax=[Ax;al,ar]
  end
  
  if or(selection=="dsp") then
    al=CreateAxesL();
    BRA_plot2(tr,res.RR_SPSDmin);
    ylabel(_("DSP"));
    ar=CreateAxesR();
    BRA_plot2(tr,res.SBP_SPSDmin);
    kplot=kplot+1;
    Ax=[Ax;al,ar]
  end
  
   if or(selection=="disp") then
     ar=CreateAxesL();
     BRA_plot2(tr,res.RR_dispmin);
     ylabel(_("Dispersion"));
     al=CreateAxesR();
     BRA_plot2(tr,res.SBP_dispmin);
     kplot=kplot+1;
     Ax=[Ax;al,ar]
   end
   //Set the "columne title
   Ax(1,1).title.text="RR";
   Ax(1,2).title.text="SBP";
   //remove the intermediate x ticks labels
   for l=1:size(Ax,1)-1
      Ax(l,1).x_ticks.labels=emptystr(Ax(l,1).x_ticks.labels);
      Ax(l,2).x_ticks.labels=emptystr(Ax(l,2).x_ticks.labels);
   end
   
   a=CreateAxesF();
   xpoly([tr(1);tr($)],ones(2,1)*res.CoherenceThreshold);
   p=gce();p.foreground=5;p.line_style=3;
   BRA_plot2(tr,res.SBP_RR_coherence,6);
   ylabel(_("SBP->RR coherence"));
   a.x_ticks.labels=emptystr(a.x_ticks.labels);

   kplot=kplot+1;
  
  a=CreateAxesF();
  BRA_plot2(tr,res.SBP_RR_gain,6);
  ylabel(_("SBP->RR gain"));
 
  drawnow()
  sda();
  fig=gcf();
  fig.event_handler="BaroreflexPlotHandler";
  fig.event_handler_enable='off';
 
  
  step=round((tbounds(2)-tbounds(1))/10);
  ud=fig.user_data;
  if typeof(ud)<>"st" then
    ud=struct();
  end
  ud.type="BaroreflexPlot";
  ud.scroll=[0,step,tbounds']
  set(fig,"user_data",ud);
  submenus=[_("Set scroll length"); _("Scroll On");_("Scroll Off")];
  delmenu(fig.figure_id,_("&Scroll"))
  addmenu(fig.figure_id,_("&Scroll"),submenus,list(2,"BaroreflexScrollMenu"))
  fig.immediate_drawing = idraw;
 
endfunction

function BaroreflexScrollMenu(k,win)
  //Utilitary function for MultiChannelPlot function
  fig=get_figure_handle(win)
  scf(fig)
  select k
  case 1  then //set Scroll length
    C=fig.children;
    C=C(C.type=="Axes");
    ud=fig.user_data;
    count=ud.scroll(1);
    step=ud.scroll(2);
    [ok,l]=uigetlength(_("Give the length of the scrolling window"),step) 
    if ~ok then return,end
    ud.scroll(1:2)=[floor(count*step/l),l];
    set(fig,"user_data",ud);
  case 2  then //Scroll On
    ud=fig.user_data;
    fig.event_handler_enable='off';
    fig.event_handler="BaroreflexPlotHandler";
    fig.event_handler_enable='on';
    xinfo(_("select time window with left and right directionnal arrows, q for full view"))
  case 3  then //Scroll Off
    fig.event_handler_enable='off';
    xinfo("")
  end
endfunction  
 

function BaroreflexPlotHandler(win, x, y, ibut)
  if and(ibut<>[37 39 113]) then return,end
  fig=get_figure_handle(win)
  fig.event_handler_enable='off';
  ud=fig.user_data
 
  zoom_box=[]
  C=fig.children;
  C=C(C.type=="Axes");
 

  select ibut
  case 37 then //scroll left
    l=ud.scroll(2);
    count=ud.scroll(1);
    xbounds=ud.scroll(3:4);
    drawlater()
    if count>0 then
      for k=1:size(C,'*')
        ax=C(k);
        zb([1 3])=xbounds(1)+(count-1)*l+[0 l];
        zb([2 4])=ax.data_bounds(:,2)'
        ax.zoom_box=zb;
        if ax.auto_ticks(1)=="off" then
          ax.auto_ticks(1)="on";//used to recompute grids positions 
          ax.x_ticks.labels=emptystr(ax.x_ticks.labels)
        end
      end
      ud.scroll(1)=count-1
    end
    drawnow()
  case 39 then //scroll right
    l=ud.scroll(2);
    count=ud.scroll(1);
    xbounds=ud.scroll(3:4);
    drawlater()
    if xbounds(1)+(count+1)*l<xbounds(2) then
      for k=1:size(C,'*')
        ax=C(k)
        zb([1 3])=xbounds(1)+(count+1)*l+[0 l];
        zb([2 4])=ax.data_bounds(:,2)';
        ax.zoom_box=zb;
        if ax.auto_ticks(1)=="off" then
          ax.auto_ticks(1)="on";//used to recompute grids positions 
          ax.x_ticks.labels=emptystr(ax.x_ticks.labels)
        end
      end
      ud.scroll(1)=count+1
    end
    drawnow()
  case 113 then //initial view
    drawlater(); 
    for k=1:size(C,'*')
      ax=C(k)
      ax.zoom_box=[];
      if ax.auto_ticks(1)=="off" then
        ax.auto_ticks(1)="on";//used to recompute grids positions 
        ax.x_ticks.labels=emptystr(ax.x_ticks.labels)
      end
    end
    drawnow()
    ud.scroll(1)=0
  end
  drawnow()
  set(fig,"user_data",ud)
  fig.event_handler_enable='on';
endfunction

function BRA_plot2(x,y,style)
  if argn(2)<3 then style=2,end
  if style==6 then
    //workaround to avoid scilab-5.3.3 bar plot mode bug
    y(isnan(y))=0;
  end
  c=[2 22 28 5];
  a=gca();
  mny=min(y(2:$,:))*0.9;mxy=max(y(2:$,:))*1.1;
  if isnan(mny) then mny=0;mxy=1;end
  a.data_bounds=[tbounds [mny;mxy]];
  a.axes_visible='on';
  a.clip_state="clipgrf";
  n=size(y,2);
  dx=(x(2)-x(1))*0.9;
  bar_width=dx/(1+(n-1)*(1-0.4))
  for k=1:n
    xpoly(x(:),y(:,k))
    e=gce();
    e.foreground=c(modulo(k-1,4)+1);
    e.polyline_style=style;
    e.clip_state="clipgrf";
    if style==6 then
      e.line_mode="off"
      e.background=c(modulo(k-1,4)+1);
      e.bar_width=bar_width
    end
    if k==2 then e.x_shift=((k-1)*0.4*bar_width)*ones(1,size(x,'*'));end
  end
endfunction
function a=CreateAxesL()
  a=newaxes();
  a.axes_bounds=[0,o+(kplot-1)*h,1/2,h];
  a.margins(1)=0.2; 
  a.margins(2)=0.;
  a.box='on';
endfunction
function a=CreateAxesR()
  a=newaxes();
  a.axes_bounds=[1/2,o+(kplot-1)*h,1/2,h];
  a.margins(1)=0.18; 
  a.margins(2)=0.1;
  a.box='on';
endfunction
function a=CreateAxesF()
  a=newaxes();
  a.axes_bounds=[0,o+(kplot-1)*h,1,h];
  a.margins(1)=0.1; 
  a.margins(2)=0.05;
  a.box='on';
endfunction

