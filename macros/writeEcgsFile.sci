//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function writeEcgsFile(S,fileout,t0)
  if typeof(S)<>"sciecg" then
    error(msprintf(_("%s: Wrong type for argument %d: sciecg data structure expected.\n"),...
                     "writeEcgsFile",1))
  end
  if argn(2)<3 then 
    t0=zeros(1,6);
  else
    if type(t0)<>1|~isreal(t0) then
      error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n"),...
                     "sciecg",3))
    end
    if size(t0,"*")<>6 then
      error(msprintf(_("%s: Wrong size for input argument #%d: A %d elements array expected.\n"),...
                     "sciecg",3,6))
    end
  end
  [Nsamples, Nchannels]=size(S)
  out=writeEcgsFileInfo(fileout,Nchannels,Nsamples,S.fs,t0)
  mput(S.sigs(:),'d',out)
  mclose(out)
endfunction
