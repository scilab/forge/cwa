//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ecgsCat(f1,f2,fout)
  in1=mopen(f1,'rb');
  BlockLength=3600*1000/2; //une demie heure
  if ascii(mget(9,'c',in1))<>"ECGScilab" then
    error("Unexpected file type")
  end
  in2=mopen(f2,'rb');
   
  if ascii(mget(9,'c',in2))<>"ECGScilab" then
    error("Unexpected file type")
  end
  out=mopen(fout,'wb')
  
  t01=mget(6,"s",in1);
  Nchannels1=mget(1,'i',in1);
  Nsamples1=mget(1,'i',in1);
  fe1=mget(1,'f',in1);

  t02=mget(6,"s",in2);
  Nchannels2=mget(1,'i',in2);
  Nsamples2=mget(1,'i',in2);
  fe2=mget(1,'f',in2);

  if or(t01<>t02)|Nsamples1<>Nsamples2|fe1<>fe2 then
    error("incompatible input files")
  end
 
  Nsamples=Nsamples1;
  BlockLength=min(Nsamples,3600*1000/2); //une demie heure
  mput(ascii("ECGScilab"),'c',out)
  mput(t01,'s',out);
  mput(Nchannels1+Nchannels2,'i',out)
  mput(Nsamples,'i',out)
  mput(fe1,'f',out)

  winH=mywaitbar("open",_("Processing"));
  //Boucle sur les blocs
  Nr=0;
  Nb=ceil(Nsamples/BlockLength);
  for b=1:Nb
    mprintf("Traitement du bloc N°%d sur %d\n",b,Nb)
    n1=Nchannels1*min(BlockLength,Nsamples-Nr)
    n2=Nchannels2*min(BlockLength,Nsamples-Nr)
    mput([matrix(mget(n1,'d',in1),Nchannels1,-1)
       matrix(mget(n2,'d',in2),Nchannels2,-1)],"d",out)
    Nr=Nr+BlockLength;
    mywaitbar("update",b/Nb,winH);
  end
  mywaitbar("close",winH);
  mclose(in1); mclose(in2);
  mclose(out);

endfunction
