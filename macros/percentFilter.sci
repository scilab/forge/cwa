function outliers=percentFilter(s,perLimit)
// Reference: 
// Clifford, G. (2002). "Characterizing Artefact in the Normal 
// Human 24-Hour RR Time Series to Aid Identification and Artificial 
// Replication of Circadian Variations in Human Beat to Beat Heart Rate 
// using a Simple Threshold."
//
// Aubert, A. E., D. Ramaekers, et al. (1999). "The analysis of heart 
// rate variability in unrestrained rats. Validation of method and 
// results." Comput Methods Programs Biomed 60(3): 197-213.      
  if perLimit>1 
    perLimit=perLimit/100; //assume incorrect input and correct it.
  end
        
  outliers=%f(ones(s)); //preallocate        
  pChange=abs(diff(s))./s(1:$-1); //percent change from previous
  //find index of values where pChange > perLimit
  outliers(2:$) = (pChange >perLimit);
endfunction
