//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [m,n]=%sciecg_size(S,varargin)
  if argn(1)==1 then
    m=size(S.sigs,varargin(:))
  else
    [m,n]=size(S.sigs)
  end
endfunction
