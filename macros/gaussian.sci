function [y,dydsigma,dydmu,dyda,dydb]=gaussian(sigma,mu,a,b,x)
//y:=b+a*exp(-(x-mu)^2/(2*sigma^2))
  dyda=exp(-(x-mu).^2/(2*sigma^2));
  y=a*dyda+b;
  if argn(1)>1 then
    dydmu=(-mu+x).*(a*dyda/sigma^2);
    dydsigma=dyda.*(-mu+x).^2*(a/sigma^3);
    dydb=1;
  end
endfunction
