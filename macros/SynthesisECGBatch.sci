//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function SynthesisECGBatch(filein,fileout)
  BlockLength=10000;
  in=mopen(filein,'rb');
  if argn(2)<2 then
    [path, fname, extension] = fileparts(filein)
    fileout=strsubst(filein,extension,"_l.ecgs")
  else
    if filein==fileout then
      error(msprintf(_("%s: Incompatible input arguments #%d and #%d: input and output filenames must be different\n"),"SynthesisECGBatch",1,2))
    end
  end
  if ascii(mget(9,'c',in))<>"ECGScilab" then
    mclose(in)
    error(msprintf(_("%s: Wrong value for input argument #%d: Unexpected file type"),"SynthesisECGBatch",1))
  end
  [fe,Nchannels, Nsamples,t0]=ecgsFileInfo(in);
  out=writeEcgsFileInfo(fileout,1,Nsamples,fe,t0);
 
  
  //Detrend window
  nf=2*fe;
  if int(nf/2)*2<>nf then nf=nf+1;end
  w=window("hn",nf);w=w/sum(w);


  first=%t;
  winH=mywaitbar("open",_("Synthesis ECG generation"));
  //Boucle sur les blocs
  Nr=0;
  Nb=ceil(Nsamples/BlockLength);
  for b=1:Nb

    n=Nchannels*min(BlockLength,Nsamples-Nr);
    S=sqrt(sum(matrix(mget(n,'d',in),Nchannels,-1).^2,1)/Nchannels);
    Nr=Nr+BlockLength;
    mput(S,"d",out);
    mywaitbar("update",b/Nb,winH);
  end
  mywaitbar("close",winH);
  mclose(in);
  mclose(out);
endfunction
