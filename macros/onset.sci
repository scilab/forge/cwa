//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function onind = onset(sig, fs, Rpeaks, SlopeFactor)
//ONSET bell-shaped wave onset detection
//
//onind = onset(sig, fs, Rpeaks, SlopeFactor)
//
//  sig: signal to be processed (column vector)
//  fs: sampling frequency
//  Rpeaks: indexes of markers per cycle
//  SlopeFactor: normalized slope of the detection point, default value = 1
//  onind: onset indexes

// Author: Qinghua Zhang
// Copyright 2011 INRIA

if argn(2)<4
  SlopeFactor = 1;
end
 
N=length(sig);

if or(Rpeaks>N)
  error('Systolic peak indices exceed signal sample length.')
end

 
betaline = [1.89*SlopeFactor; -1];
betaline = betaline/sqrt(betaline'*betaline); // Normalize

Nc = length(Rpeaks);
onind = zeros(Nc-1,1);
for kc=2:Nc
  [w, minind] = min(sig(Rpeaks(kc-1):Rpeaks(kc)));
  minind = max(minind) + Rpeaks(kc-1) - 1;

  Nlr = Rpeaks(kc)-minind+1;  
  
  locvx = (1:Nlr)'/fs;
  locvy = sig(minind:Rpeaks(kc));
  locvy = locvy - locvy(1);   // Translate
  locvy = locvy / locvy(Nlr); // Normalize
  
  projdist = [locvx locvy]*betaline;
  
  [w, maxind] = max(projdist);
  onind(kc-1) =  minind + maxind - 1;
end

endfunction

