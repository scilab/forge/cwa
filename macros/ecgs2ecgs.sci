//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ecgs2ecgs(filein,fileout,from,N,sigsel)
  //cr�e un fichier ecg a partir d'un extrait d'un fichier ecg
  //indsel : assumed to be in strictly increasing order
  if argn(2)<4 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),"ecgs2ecgs",4,5))
  end
  if filein==fileout then
    error(msprintf(_("%s: Incompatible input arguments #%d and #%d: input and output filenames must be different\n"),"ecgs2ecgs",1,2))
  end
  in=mopen(filein,"rb");
  if ascii(mget(9,'c',in))<>"ECGScilab" then
    mclose(in)
    error(msprintf(_("%s: Wrong value for input argument #%d: Unexpected file type"),"ecgs2ecgs",1))
  end
  [fe,Nchannels, Nsamples,t0]=ecgsFileInfo(in)
  
  
  if argn(2)<>5|or(size(sigsel)==-1) then 
    sigsel=1:Nchannels;
  else
    if or(sigsel<1|sigsel>Nchannels) then
      mclose(in)
      error(msprintf(_("%s: Wrong values for input argument #%d: Elements must be in the interval ""[%s, %s].\n"),"ecgs2ecgs",5,"1",string(Nchannels)))
    end
  end
  if from<1|from>Nsamples then
    mclose(in)
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval [%d, %d].\n"),"ecgs2ecgs",3,1,Nsamples))
  end
  if N<=0|from+N>Nsamples then
    mclose(in)
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be in t"+...
                     " he interval [%d, %d].\n"),"ecgs2ecgs ",4,1,Nsamples-from))
  end
  if from<>1&and(t0(1:3)<>0) then
    d=datenum(t0)+round((from-1)/(fe*24*3600))
    t0=datevec(d)
  end
  
  out=writeEcgsFileInfo(fileout,Nchannels,N,fe,t0)
  if from>1 then
    //skip the first samples
    mseek_big(mtell(in)+(from-1)*Nchannels*8,in)
  end
  BlockLength=1000;
  winH=mywaitbar("open",_("Processing "));
  Nb=ceil(N/BlockLength);
  for k=1:Nb
    s=matrix(mget(Nchannels*BlockLength,'d',in),Nchannels,-1);
    s=s(sigsel,:);
    mput(s(:),'d',out)
    mywaitbar("update",k/Nb,winH);
  end
  mclose(in)
  mclose(out)
  
  mywaitbar("close",winH);
endfunction
