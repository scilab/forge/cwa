// This file is part of the Cardiovascular Wawes Analysis toolbox
// Copyright (C)  - University of Rochester Medical Center (URMC). 
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [S,head]=readISHNE(fileName, startOffset, numSample)
// Read ISHNE ecg file : 
//  function [ecg [,header]]=read_ishne(fileName [, startOffset [,numSample]);    
// Input--------------------------------------------------------       
//  fileName : the ishne filename including the path
//  startOffset: the start offset (in number of sample) to read ecg, default value is 0
//  numSample:      length (in number of sample) of ecg to read. By
//  default the ecg data is read up to the end
// Ouput--------------------------------------------------------
//  ecg : an 2D array, each column correspond to a lead
//  head : the file header (see readISHNEHeader)
  //
//This file is derived from the Matlab function found at 
//  http://thew-project.org/code_library.htm

  if argn(2)<3 then 
    numSample=-1;
  else
    if type(numSample)<>1|size(numSample,'*')<>1|numSample<=0 then
      mclose(fid);
      error(msprintf("%s: Wrong value for input argument #%d: A positive integer value expected.\n"),"readISHNE",3)
    end
  end
  if argn(2)<2 then  
    startOffset=0;
    if type(startOffset)<>1|size(startOffset,'*')<>1|startOffset<0 then
      mclose(fid);
      error(msprintf("%s: Wrong value for input argument #%d: A positive integer value expected.\n"),"readISHNE",2)
    end
  end
  fid=mopen(fileName,'rb');
  //Magic number
  magicNumber = ascii(mget(8,"uc",fid));
  if magicNumber<>"ISHNE1.0" then
    error("The given file is not a ISHNE1.0 file")
  end

  head=readISHNEHeader(fid);

  // read variable_length block
  if head.Var_length_block_size > 0
    varblock = mget(head.Var_length_block_size,"c",fid);
  end
 // get data at start
  offset = startOffset*(head.nbLeads*2); // in  bytes

  if numSample==-1 then
     mseek(0,fid, 'end');
     L=mtell(fid);
     numSample=int((L-offset)/(2*head.nbLeads))
  end
  mseek(head.Offset_ECG_block+offset,fid, 'set');
  Resolution=head.Resolution(1:head.nbLeads)/1000;//convert in micro volts
  // read ecg signal
  S=sciecg(head.Sampling_Rate,...
          matrix(mget(head.nbLeads*numSample,"s",fid),head.nbLeads,-1)'*diag(1.0./Resolution));
  mclose(fid);
endfunction
