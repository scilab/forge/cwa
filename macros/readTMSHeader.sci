//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [HDR,f]=readTMSHeader(u)
//http://www.tmsi.com/downloads/tms_read.m
  opened=type(u)==1
  if ~opened then
    u=mopen(u,'rb');
  end
  //lecture du header
  HDR.ID= ascii(mget(31,'uc',u))
  if part(HDR.ID,1:16)<>"POLY SAMPLE FILE" then
    mclose(u)
    error(msprintf(_("%s: Wrong value for input argument #%d: a %s file format expected.\n"),...
                   "readTMSHeader",1,"TMS32/Poly5"))
  end
  HDR.Version=mget(1,'s',u)
  if HDR.Version<>203 then
    mseek(0,u,'set')
    HDR.ID= ascii(mget(32,'uc',u))
    HDR.Version=mget(1,'s',u)
  end
  HDR.MeasurementName=ascii(mget(81,'uc',u))
  HDR.SampleRate=mget(1,'s',u)
  HDR.StorageRate=mget(1,'s',u)
  HDR.StorageType=mget(1,'uc',u)
  HDR.Nchannels=mget(1,'s',u)//number of signals
  HDR.Nsamples=mget(1,'i',u)
  mget(1,'i',u);
  HDR.Date=mget(7,'s',u)// start date
  f.NB=mget(1,'i',u)// number of blocks
  f.SPR=mget(1,'us',u)//number of sample period per block
  bpb =mget(1,'us',u)+86 //size of a block in byte
  DeltaCompression=mget(1,'s',u)
  mget(64,'uc',u);//trailing zeros
  if HDR.Version==203 then
    HeadLen = 217 + HDR.Nchannels*136;
  else
    HeadLen = 218 + HDR.Nchannels*136;
  end
  aux=0

  for k=1:HDR.Nchannels,
    c =mget(1,'uc',u)
    tmp=ascii(mget(40,'uc',u))
    if part(tmp,1:4)=='(Lo)' then
      HDR.Labels(k-aux) = stripblanks(part(tmp,6:c));
      f.GDFTYP(k-aux)  = 16;
      
    elseif  part(tmp,1:4)=='(Hi)' then
      //Similar to previous '(Lo)' ignore it
      aux = aux + 1;
    else
      HDR.Labels(k-aux)  = stripblanks(part(tmp,1:c));
      f.GDFTYP(k-aux) = 3;
    end
    tmp=mget(4,'uc',u)   
    c=mget(1,'uc',u)  
    tmp=ascii(mget(10,'uc',u))
    HDR.UnitName(k-aux) = stripblanks(part(tmp,1:c));
    f.UnitLow(k-aux,1) = mget(1,'f',u)			
    f.UnitHigh(k-aux,1) = mget(1,'f',u)
    f.ADCLow(k-aux,1)  = mget(1,'f',u)
    f.ADCHigh(k-aux,1)  = mget(1,'f',u)
    IndexSignalList = mget(1,'s',u)
    CacheOffset=mget(1,'s',u)
    Reserved2 = mget(60,'uc',u)
  end
  
  HDR.Nchannels  = HDR.Nchannels-aux; 
  f.Nchannels=HDR.Nchannels;
  f.Nsamples=HDR.Nsamples //number of sample periods
    
  f.NR=0;
  if ~opened then mclose(u);end
endfunction
