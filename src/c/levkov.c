#include "math.h"
int Levkov(double *src,double *des,int N,double Threshold,int Nsamples, double *temp)
/* ecgpuwave */
/* http://www.pudn.com/downloads95/sourcecode/math/biometrics/detail386898.html*/
{
  int A=1,i=0,j=0,k=0;
  int Ns2 = 0;
  double sum=0.0;
  double d1,d2;

  if(N%2==0) 
    Ns2 = N/2-1;
  else
    Ns2 = (N-1)/2;
  i=0;
  for(k=0;k<N;k++) temp[k]=0.0;
  while(i<Nsamples-N-1) {
    if(j>=N) j=0;
    d1=fabs(src[i+N]-src[i]);
    d2=fabs(src[i+N+1]-src[i+1]);
    if(fabs(d1-d2)<Threshold) {/*linear interval detected*/
      A--;
      if(A==0){
        A=1;
        sum = 0;	
        for(k=0;k<N;k++) sum+=src[i+k];
        if(N%2==0)  sum-=(src[i+N]-src[i])/2;
        des[i+Ns2]=sum/N;
        temp[j]=src[i+Ns2]-des[i+Ns2];
      }
      else {
        des[i+Ns2]=src[i+Ns2]-temp[j];
      }
    }
    else{ /*non linear part*/
      A=N;
      des[i+Ns2]=src[i+Ns2]-temp[j];
    }
    i++;
    j++;
  }
  /*Copy first N/2 src samples*/
  for (i=0;i<Ns2;i++) des[i]=src[i];

  /*Copy  last src samples */ 
  for (i=Nsamples-N-1+Ns2;i<Nsamples;i++) des[i]=src[i];
  return 0;
}
