f_ech=4;//hz
t=0:0.25:600; //4hz sampling
N=size(t,'*');
tt=1:N;
Fmin=(0.09-0.01)/f_ech;Fmax=(0.09+0.01)/f_ech;
[LF,IFreq_ref]=fmsin(N,Fmin,Fmax,150,1,(Fmin+Fmax)/2);
IFreq_ref=IFreq_ref';
clf;
plot(tt,IFreq_ref,'b')
// amplitude modulation
IampLF=4;
sig=IampLF*real(LF)';
clear options
fmin=0.01;
options.frequencywindowlength=73;
options.timewindowlength=37;
options.frequencybins=128;

//check effect of lpwl on delay
lpwl=55;
LP=wfir("lp",lpwl,[fmin 0],"hm",[0 0]);
options.lowpass=LP;
[T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
dly=(lpwl-1)/2
plot(tt(1:$-dly),IFreq(dly+1:$-dly),'r')
lpwl=85;
LP=wfir("lp",lpwl,[fmin 0],"hm",[0 0]);
options.lowpass=LP;
[T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
dly=(lpwl-1)/2
plot(tt(1:$-dly),IFreq(dly+1:$-dly),'m')

//check effect of fwl on delay
halt
clf;
//plot(tt,IFreq_ref,'b')
fwl=85;
options.frequencywindowlength=fwl;
[T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
plot(T,IFreq,'b')


fwl=73;
options.frequencywindowlength=fwl;
[T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
plot(T,IFreq,'r')

fwl=69;
options.frequencywindowlength=fwl;
[T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
plot(T,IFreq,'m')


//check effect of twl  on delay
halt
clf;

twl=37
options.timewindowlength=twl;
[T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
plot(T,IFreq,'b')

twl=57
options.timewindowlength=twl;
[T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
plot(T,IFreq,'r')

twl=77
options.timewindowlength=twl;
[T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
plot(T,IFreq,'m')

//check effect of nfb on delay

halt
clf;

nfb=64;
options.frequencybins=nfb;
[T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
plot(T,IFreq,'b')

nfb=128;
options.frequencybins=nfb;
[T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
plot(T,IFreq,'r')

nfb=256;
options.frequencybins=nfb;
[T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
plot(T,IFreq,'m')
