//test with
//  - Piecewise constant amplitude
//  - linearly modulated frequency
//---------------------------------
CurPath=get_absolute_file_path("testSPWVDpars.sce")
// Create a frequency modulated signal
fw=0.3;

u=mopen(CurPath+"testSPWVDpars.res","w");
mfprintf(u,"f0;tmax;npp;N;; fmin;lpwl;;twl;fwl;nf;;fmin;lpwl;;twl;fwl;nf;;e\n")
R=[];
C=[];
for f0=[1 5 11 20]
  for tmax=[10 15]
    for npp=[25 55]
      freq_sampling=f0*npp;
      // Time instants
      t=0:1/freq_sampling:tmax;
      N=size(t,'*');
      // Frequency modulation
      IFreq_ref=f0*(1+linspace(-fw,fw,N));
      // Amplitude modulation
      n1=round(N/2);
      A=[linspace(1,2,n1) linspace(2,1,N-n1)];
      // Input signal
      sig=A.*cos(2*%pi*IFreq_ref.*t)+0.1*rand(t);
      res=[];
      d=[f0, tmax, npp, N];
      for fmin=[0.0005 0.001 0.002 0.005 0.01 0.02]
        for lpwl=[95 115]
          LP=wfir("lp",lpwl,[fmin 0],"hm",[0 0]);
          clear options;
          options.lowpass=LP;
          for fwl=69:2:81
            options.frequencywindowlength=fwl;
            for twl=[27 37 57]
              options.timewindowlength=twl;
              for nf=2.^[5:7]
                options.frequencybins=nf;
                // Call the SPWVD demodulation
                [T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
                // Take sampling frequency into account
                IFreq=IFreq*freq_sampling;
                nd=round(N/8); //to remove head and tail for error computation
                e=norm((IFreq(delay+nd:$-nd)-IFreq_ref(nd:$-nd))./IFreq_ref(nd:$-nd),'inf');
                res=[res;
                     fmin, lpwl, twl,  fwl, nf, e];
              end
            end
          end
        end
      end
      for k=1:5,d=[d corr(res(:,6),res(:,k),1)];end
      [m,k]=min(res(:,6));
      d=[d res(k,:)]
      mfprintf(u,"%d;%d;%d;%d;;%.4f;%.4f;;%.4f;%.4f;%.4f;;%.4f;%d;;%d;%d;%d;;%.4f\n",d)
      C=[C;d];
    end
  end
end
mclose(u)
