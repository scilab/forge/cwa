//This script compares the results of ECGRpeaks and ECGTwaveEnds against the
//previously computed results stored in qtdb/<record>.QZref
function checkQZref(name)
  margin=4.8;//=1200 echantillons
  ann=wfdbReadAnnotations(name,"q1c");
  tend=find(ann(2,:)==40&ann(5,:)==2)
  tmin=ann(1,tend(1))-margin;
  tmax=ann(1,tend($))+margin;
  [sig,fe,n]=wfdbReadSamples(name,%f,[tmin,tmax]);
  S1=sig(1,:)';
  S2=sig(2,:)';
  S1 = filtbyfft(S1-mean(S1),fe, [0.5 250]);
  S2 = filtbyfft(S2-mean(S2),fe, [0.5 250]);
  kRp1=ECGRpeaks(S1,fe);
  kTe1=ECGTwaveEnds(S1, fe,kRp1)'; 
  kRp2=ECGRpeaks(S2,fe);
  kTe2=ECGTwaveEnds(S2, fe,kRp2)'; 
  load(name+".QZref")
  mprintf("%d,%s:\t",kd,name)
  if size(kTe1_ref,'*')<> size(kTe1,'*') then
    mprintf("Te1 size differ"),ok=%f
  end
  if size(kTe2_ref,'*')<> size(kTe2,'*') then
    mprintf("Te1 size differ"),ok=%f
  end
  if size(kRp1_ref,'*')<> size(kRp1,'*') then
    mprintf("Rp1: szref=%d,sz=%d\t", size(kRp1_ref,'*'),size(kRp1,'*'))
  else
    [mR1,kR1]=max(abs(kRp1_ref-kRp1));
    if mR1<>0 then  mprintf("Rp1: max err=%d index=%d\t",mR1,kR1);end
  end
  if size(kRp2_ref,'*')<> size(kRp2,'*') then
    mprintf("Rp2: szref=%d,sz=%d\t", size(kRp2_ref,'*'),size(kRp2,'*'))
  else
    [mR2,kR2]=max(abs(kRp2_ref-kRp2));
    if mR2<>0 then  mprintf("Rp2: [%d %d]\t",mR2,kR2);end
  end
  
  
  ind_ref=find(kTe1_ref<>-1); ind=find(kTe1<>-1)
  if size(ind_ref,'*')<> size(ind,'*') then
    mprintf("Te1: #missed %d -> %d\t",size(ind_ref,'*'),size(ind,'*'));
  elseif or(ind_ref<>ind) then
     mprintf("Te1: missed differ\t");
  else
    [mT1,kT1]=max(abs(kTe1_ref(ind)-kTe1(ind)));
    if mT1<>0 then  mprintf("Te1: max err=%d index=%d\t",mT1,kT1);end
  end

  ind_ref=find(kTe2_ref<>-1); ind=find(kTe2<>-1)
  if size(ind_ref,'*')<> size(ind,'*') then
    mprintf("Te2: #missed %d -> %d\t",size(ind_ref,'*'),size(ind,'*'));
  elseif or(ind_ref<>ind) then
     mprintf("Te2: missed differ\t");
  else
    [mT2,kT2]=max(abs(kTe2_ref(ind)-kTe2(ind)));
    if mT2<>0 then  mprintf("Te2: max err=%d index=%d\t",mT2,kR2);end
  end

  mprintf("\n")
endfunction
datapath=get_absolute_file_path("checkQZref.sce");
if ~isdir(datapath+"qtdb/") then
  if exec(datapath+"qtdbGet.sce","errcatch")<>0 then
    error("Downloading qtdb database failed")
  end
end
datapath=datapath+"qtdb/"
dnames=gsort(strsubst(ls(datapath+"*.QZref"),".QZref",""),'g','i');
Nnames=size(dnames,'*');
for kd=1:Nnames
  if  and(fileparts(dnames(kd),'fname')<>["sel35" "sel37"]) then
    checkQZref(dnames(kd))
  end
end
