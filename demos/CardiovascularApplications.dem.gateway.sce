// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function subdemolist = demo_gateway()

  demopath = get_absolute_file_path("CardiovascularApplications.dem.gateway.sce");
  subdemolist = [
      "Spectrum analysis"                                   "CV_TimeFrequency.dem.gateway.sce"
      "RR analysis with CDM and SPWVD methods comparison"   "CV_CDM_SPWVD.dem.gateway.sce"
      "RR analysis with Complex DeModulation"               "CV_CDM.dem.gateway.sce"
      "RR analysis with Smoothed Pseudo Wigner-Ville"       "CV_SPWVD.dem.gateway.sce"
      "Baroreflex analysis"                                 "Baroreflex.dem.gateway.sce"
                ]
 subdemolist(:,2) = demopath + subdemolist(:,2);
 
endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
