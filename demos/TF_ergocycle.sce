// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

scriptfilename = "TF_ergocycle.sce";
datapath   = get_absolute_file_path(scriptfilename);
datafile   = datapath+"DATA/cycle_spont";

d=read(datafile,-1,7);
freq_sampling=8;
RR=d(:,1);

f1=scf(100001);clf;f1.figure_position=[0 0];f1.figure_size=[524 514];
f1.figure_name="TimeFrequency RR ergocycle";
TimeFrequencyTool(RR,freq_sampling) 
