<?xml version="1.0" encoding="UTF-8"?>
<!--
 This file is part of the Cardiovascular Wawes Analysis toolbox
 Copyright (C) 2014 - INRIA - Serge Steer
 This file must be used under the terms of the CeCILL.
 This source file is licensed as described in the file COPYING, which
 you should have received as part of this distribution.  The terms
 are also available at
 http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="ecgsyn">
  <refnamediv>
    <refname>ecgsyn</refname>
    <refpurpose>A realistic ECG signal generator.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[s,ipeaks] = ecgsyn(arguments)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>arguments</term>
        <listitem>
          <para>
            A sequence of pairs (argument_name, argument_value) where
            argument_name is character string with possible values:
            "fs", "n", "anoise", "hrmean", "hrstd", "lfhfratio", "ti",
            "ai", "bi", "aresp", "fresp".
          </para>
          <itemizedlist>
            <listitem><para>fs : a positive number, the sampling
            frequency of the generated ECG in Hz. The default value
            is 256 </para></listitem>
            <listitem><para>n: a positive integer, the approximative
            number of beats. The default value is 256
            </para></listitem>
            <listitem><para>anoise: a positive number, the measurement
            noise amplitude in mV. The default value is 0.
            </para></listitem>
            <listitem><para>hrmean: a positive number, the mean
            heart rate in beats per minute. The default value is
            60.  </para></listitem>
            <listitem><para>hrstd: a positive number, the standard
            deviation of heart rate in beats per minute. The
            default value is 1.  </para></listitem>
            <listitem><para></para></listitem>
            <listitem><para>lfhfratio: a positive number, the LF/HF
            ratio. The default value is 0.5.</para></listitem>
            <listitem><para>ti: a 5 elements array, the angles of the P, Q, R, S, T 
            peaks in degree. The default value is [-70 -15 0 15
            100].</para></listitem>
            <listitem><para>ai: a 5 elements array, the z-position
            of the P, Q, R, S, T peaks. The default value is [1.2 -5 30 -7.5
            0.75].</para></listitem>
            <listitem><para>bi: a 5 elements array, the Gaussian
            width of the P, Q, R, S, T peaks. The default value is [0.25 0.1 0.1 0.1
            0.4].</para></listitem>
            <listitem><para>aresp: a positive number, the amplitude of
            modulation due to sinusal arythmia. The default value is
            0.005.</para></listitem>
            <listitem><para>aresp: a positive number, the frequency in Hz of
             sinusal arythmia. The default value is
            0.25.</para></listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>s</term>
        <listitem>
          <para>
            a real array, the ECG samples
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>ipeaks</term>
        <listitem>
          <para>
            a real array, with same length as s. The P, Q, R, S, T
            peaks locations are characterized by respectively a value
            1, 2, 3, 4 ou 5 in ipeaks. For example, the R peaks index
            can be obtained using kRp=find(ipeaks==3).
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      This function produces a synthetic ECG according to the given
      parameters. It also produces the exact location of the P, Q, R,
      S, T peaks in the generated ECG.
    </para>
 
  </refsection>
 
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    [s,ipeaks]=ecgsyn("n",10,"fs",500,"ai",[0.5 -3   30 -2 0.25]);
    kRp=find(ipeaks==3);
    kTp=find(ipeaks==5);
    t=(0:size(s,"*")-1)'/500;
    clf();plot(t,s,'b',t(kRp),s(kRp),"+r",t(kTp),s(kTp),"+g");
    xlabel(_("Time (s)"))
    legend("ECG","R peaks", "T peaks");
    ]]></programlisting>
    <scilab:image><![CDATA[
    [s,ipeaks]=ecgsyn("n",10,"fs",500,"ai",[0.5 -3   30 -2 0.25]);
    kRp=find(ipeaks==3);
    kTp=find(ipeaks==5);
    t=(0:size(s,"*")-1)'/500;
    clf();plot(t,s,'b',t(kRp),s(kRp),"+r",t(kTp),s(kTp),"+g");
    xlabel(_("Time (s)"))
    legend("ECG","R peaks", "T peaks");

    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="ECGWaveReconstruct" >ECGWaveReconstruct</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Patrick McSharry and Gari Clifford, Matlab version</member>
      <member>Serge Steer, INRIA, Scilab port</member>
    </simplelist>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
         McSharry PE, Clifford GD, Tarassenko L, Smith L. <ulink
         url="http://www.physionet.org/physiotools/ecgsyn/paper/">A
         dynamical model for generating synthetic electrocardiogram
         signals</ulink>. IEEE Transactions on Biomedical Engineering
         50(3): 289-294; March 2003.
       </para>
       <para>
         <ulink
         url="http://www.physionet.org/physiotools/ecgsyn">http://www.physionet.org/physiotools/ecgsyn</ulink>
       </para>
     </refsection>
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function  added</revdescription>
        </revision>
      </revhistory>
    </refsection>
  <refsection>
     <title>Used Functions</title>
       <para>
         The ECG generation is done using the <link
         linkend="scilab.help/ode" type="scilab">ode</link> function
         applyed to the external function ecgsyn_ode (coded in C)
       </para>
     </refsection>
</refentry>
