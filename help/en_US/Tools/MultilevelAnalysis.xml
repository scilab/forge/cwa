<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2014 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="MultilevelAnalysis">
  <refnamediv>
    <refname>MultilevelAnalysis</refname>
    <refpurpose>Computes the large deviations spectrum of a signal.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[alfa,F] = MultilevelAnalysis(Sig,N [,options])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>Sig</term>
        <listitem>
          <para>
            A real vector, the signal samples.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>N</term>
        <listitem>
          <para>
            a vector with integer values, the dyadic levels requested.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>options</term>
        <listitem>
          <para>
             a sequence key, value:

          </para>
          <variablelist>
            <varlistentry>
              <term>"Q"</term>
              <listitem>
                <para>
                  the associated value must be a real vector, the default value is 
  Q = logspace(log10(0.01),log10(15),100) ; Q = [-Q($:-1:1) 0 Q] ;
 
                </para>
              </listitem>
            </varlistentry>
            <varlistentry>
              <term>"eps_flag"</term>
              <listitem>
                <para>
                  The associated value must be a number with possible values:
                </para>
                  <itemizedlist>
                    <listitem>
                      <para>
                        1: eps computed using log(log(n)) factor 
                      </para>
                    <listitem>
                      <para>
                        2: eps computed using  log(n) factor 
                      </para>
                    </listitem>
                    </listitem>
                    <listitem>
                      <para>
                        eps computed using  factor 1 
                      </para>
                    </listitem>
                  </itemizedlist>
                  <para>
                  The default value is 1
                </para>
               
              </listitem>
            </varlistentry>
             <varlistentry>
              <term>"t"</term>
              <listitem>
                <para>
                  The associated value must be a real vector with
                  identical size as Sig. This option allows to specify
                  time instants corresponding to the Sig values. if t
                  is given the dyadic intervals are computed with
                  respect to the t values.
                </para>
                <para>
                  The default value is []
                </para>
              </listitem>
            </varlistentry>
            <varlistentry>
              <term>"xsi"</term>
              <listitem>
                <para>
                  The associated value must be a real valued, non
                  identically null, non negative function with returns
                  a value for a given dyadic interval. The default
                  value is the oscillation function:  </para>
                  <programlisting><![CDATA[
                  function r=xsi(x),r=max(x)-min(x);endfunction
                  ]]></programlisting>
              </listitem>
            </varlistentry>
 
          </variablelist>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>alpha</term>
        <listitem>
          <para>
            A length(Q) by length(N) real array.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>F</term>
        <listitem>
          <para>
            A length(Q) by length(N) real array.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
Multifractal analysis is a natural statistical and geometrical
framework to describe the roughness of a signal. Specifically, it
describes quantitatively the local variability of the roughness by
measuring the distribution at small scales of the Hölder singularities
          
    </para>
 
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
s=fscanfMat(CWApath()+"demos/datafiles/raingauge.txt"); 
[dTau,F]=MultilevelAnalysis(s,5:13);
clf;plot(dTau,F)
legend("n="+string(5:13))
    ]]></programlisting>
    <scilab:image><![CDATA[
s=fscanfMat(CWApath()+"demos/datafiles/raingauge.txt"); 
[dTau,F]=MultilevelAnalysis(s,5:13);
clf;plot(dTau,F)
legend("n="+string(5:13))
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
      <member>Julien Barral, Université Paris 13</member>
      <member>Paulo Gonçalves,INRIA </member>
      
    </simplelist>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
         <ulink
         url="http://perso.ens-lyon.fr/paulo.goncalves/pub/Barral-Goncalves.JstatPhys.2011.pdf">"On
         the Estimation of the Large Deviation Spectrum" J.Barral,
         P. Gonçaves, Springer Verlag</ulink>
       </para>
     </refsection>
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function added</revdescription>
        </revision>
      </revhistory>
    </refsection>
 
</refentry>
