<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2014 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="fit_gaussian">
  <refnamediv>
    <refname>fit_gaussian</refname>
    <refpurpose>Fit a 1D Gaussian function with experimental data.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[sigma,mu,a,b,e]=fit_gaussian(x,y [,sigma0 [,mu0]])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>x</term>
        <listitem>
          <para>
            A real vector, the discretization of the abscissa.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>y</term>
        <listitem>
          <para>
            A real vector, the discretization of the ordinates.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>sigma0</term>
        <listitem>
          <para>
            A positive scalar, the initial guess for sigma. The default value is 0.5.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>mu0</term>
        <listitem>
          <para>
            A scalar, the initial guess for the mean mu. If omitted
            the initial value is set to the x value for which abs(y)
            raise its max value.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>sigma</term>
        <listitem>
          <para>
             A positive scalar, the  computed value for sigma.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>mu</term>
        <listitem>
          <para>
            A scalar, the  computed value for mu.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>a</term>
        <listitem>
          <para>
            A real scalar, the max value of the Gaussian.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>b</term>
        <listitem>
          <para>
            A real scalar, the shift value of the Gaussian.
          </para>
        </listitem>
      </varlistentry>
 
        <varlistentry>
        <term>e</term>
        <listitem>
          <para>
            The l2 norm between the experimental data and the Gaussian
            values at the abscissa points.
          </para>
        </listitem>
      </varlistentry>
   </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
This function fits a 1D Gaussian with experimental data given by the arrays x and y.
    </para>
    <para>The Gaussian is parametrized as follow:
    <latex>
            \begin{eqnarray}
y(x)=b+a exp(-\frac{(x-\mu)^2}{(2*\sigma^2)}
    \end{eqnarray}
    </latex>
    </para>
    <para> the parameters a,b, sigma and mu are found using an optimization procedure (see <link linkend="scilab.help/optim"  type="scilab">optim</link>). </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
x = [0,0.11,0.21,0.32,0.42,0.53,0.63,0.74,0.84,0.95,1.05,1.16,...
     1.26,1.37,1.47,1.58,1.68,1.79,1.89,2];
y = [0.36,0.53,0.3,0.4,0.52,0.57,0.79,1.05,1.52,1.58,1.72,1.45,...
     1.06,0.6,0.54,0.39,0.37,0.37,0.57,0.5];  
[sigma,mu,a,b,e]=fit_gaussian(x,y)
xe=linspace(0,2,300);
ye=gaussian(sigma,mu,a,b,xe);
clf;plot(x,y,"ob",xe,ye,"r")
    ]]></programlisting>
    <scilab:image><![CDATA[
x = [0,0.11,0.21,0.32,0.42,0.53,0.63,0.74,0.84,0.95,1.05,1.16,...
     1.26,1.37,1.47,1.58,1.68,1.79,1.89,2];
y = [0.36,0.53,0.3,0.4,0.52,0.57,0.79,1.05,1.52,1.58,1.72,1.45,...
     1.06,0.6,0.54,0.39,0.37,0.37,0.57,0.5];  
[sigma,mu,a,b,e]=fit_gaussian(x,y)
xe=linspace(0,2,300);
ye=gaussian(sigma,mu,a,b,xe);
clf;plot(x,y,"ob",xe,ye,"r")

set(gcf(),"axes_size",[610,300])
    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="scilab.help/optim"  type="scilab">optim</link>
      </member>
      <member>
        <link linkend="gaussian">gaussian</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>

    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function  added</revdescription>
        </revision>
      </revhistory>
    </refsection>
  <refsection>
     <title>Used Functions</title>
       <para>
         This function is based on the <link linkend="scilab.help/optim"  type="scilab">optim</link> built-in function
       </para>
     </refsection>
</refentry>
