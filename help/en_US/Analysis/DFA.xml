<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2013 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="DFA">
  <refnamediv>
    <refname>DFA</refname>
    <refpurpose>Linear detrended fluctuation analysis.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>output = DFA(data [,L [,breakpoint]])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>data</term>
        <listitem>
          <para>
            A real 1D array, the signal.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>L</term>
        <listitem>
          <para>
            a 1D array with integer values (in increasing order), the window sizes. The default value is 4:300
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>breakpoint</term>
        <listitem>
          <para>
           a scalar with integer value, L(1:breakpoint) is used to
           compute alpha1 and L(breakpoint+1:$) is used to compute
           alpha2. The default value is 13.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>output</term>
        <listitem>
          <para>
            A struct with the following fields:
          </para>
          <itemizedlist>
            <listitem>
              <para>alpha, a polynomial of degree 1, the equation of the line in log-log plot of
              integrated data  vs all window sizes in L.</para> 
            </listitem>
            <listitem>
              <para>alpha1, a polynomial of degree 1, the equation of the line in log-log plot of
              integrated data  vs  window sizes in L(1:breakpoint).</para>
            </listitem>
            <listitem>
              <para>alpha2, a polynomial of degree 1, the equation of the line in log-log plot of
              integrated data  vs  window sizes in L(breakpoint+1:$).</para>
            </listitem>
            <listitem>
              <para>n, a row array, the window sizes for which the computaion of fluctuation has been done</para>
            </listitem>
            <listitem>
              <para>F_n, the array of root mean square fluctuations for each window size</para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      DFA is a method for determining the statistical self-affinity of a signal. 
    </para>
   
  </refsection>
 
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
load(CWApath()+"demos/datafiles/RR_free.dat");
res=DFA(RR); 
x=log10(res.n);
clf;plot(x,log10(res.F_n))
plot(x(1:13),horner(res.alpha1,x(1:13)),'r')
plot(x(14:$),horner(res.alpha2,x(14:$)),'g')
xlabel("log10(n)");ylabel("log10(F_n)")
    ]]></programlisting>
    <scilab:image><![CDATA[
load(CWApath()+"demos/datafiles/RR_free.dat");
res=DFA(RR); 
x=log10(res.n);
clf;plot(x,log10(res.F_n))
plot(x(1:13),horner(res.alpha1,x(1:13)),'r')
plot(x(14:$),horner(res.alpha2,x(14:$)),'g')
xlabel("log10(n)");ylabel("log10(F_n)")
    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="SampEn" >SampEn</link>
      </member>
      <member>
        <link linkend="timeDomainHRV" >timeDomainHRV</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA, Scilab version</member>
      <member>John T. Ramshur, University of Memphis, Department of
      Biomedical Engineering,jramshur@gmail.com, Matlab
      version</member>
    </simplelist>
  </refsection>
  <refsection>
     <title>Bibliography</title>
     <para>
       <ulink
           url="http://en.wikipedia.org/wiki/Detrended_fluctuation_analysis">http://en.wikipedia.org/wiki/Detrended_fluctuation_analysis</ulink>
     </para>
       <para>
         <ulink
         url="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC493278/pdf/1475-925X-3-24.pdf">"Heart
         rate analysis in normal subjects of various age groups",
         Rajendra Acharya U, Kannathal N, Ong Wai Sing, Luk Yi Ping
         and TjiLeng Chua</ulink>
       </para>
     </refsection>
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function added</revdescription>
        </revision>
      </revhistory>
    </refsection>

</refentry>
