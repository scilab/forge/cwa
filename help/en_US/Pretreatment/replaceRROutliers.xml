<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2013 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="replaceRROutliers">
  <refnamediv>
    <refname>replaceRROutliers</refname>
    <refpurpose>Add short description here.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[RR_out,t_out] = replaceRROutliers(t_in,RR_in,outliers,"remove")</synopsis>
    <synopsis>[RR_out,t_out] = replaceRROutliers(t_in,RR_in,outliers,"mean",winwidth)</synopsis>
    <synopsis>[RR_out,t_out] = replaceRROutliers(t_in,RR_in,outliers,"median",winwidth)</synopsis>
    <synopsis>[RR_out,t_out] = replaceRROutliers(t_in,RR_in,outliers,"spline")</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>t_in</term>
        <listitem>
          <para>
            A real 1D array, the time values associated to the beats.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>RR_in</term>
        <listitem>
          <para>
             A real 1D array, the inter beat durations
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>outliers</term>
        <listitem>
          <para>
            A boolean array with same size as t_in and RR_in.
          </para>
        </listitem>
      </varlistentry>
      
      <varlistentry>
        <term>winwidth</term>
        <listitem>
          <para>
            A positive scalar with integer value, the width of the window used by the "mean" and "median" methods.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>RR_out</term>
        <listitem>
          <para>
            The new RR signal.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>t_out</term>
        <listitem>
          <para>
            The new time array.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      This function suppresses the outliers of the RR record using different methods:
    </para>
    <itemizedlist>
      <listitem>
        <para>with "remove" option the outlier data points are
        suppressed t_out=t_in(~outliers) and
        RR_out=RR_in(~outliers).</para>
      </listitem>
     <listitem>
        <para>with "mean" option the outlier data points are replaced by the mean of the winwidth neighboring points
        </para>
      </listitem>
     <listitem>
        <para>with "median" option the outlier data points are replaced by the median of the winwidth neighboring points
        </para>
      </listitem>
     <listitem>
        <para>with "spline" option the outlier data points are replaced by interpolated values using a global spline fitting.
        </para>
      </listitem>
    </itemizedlist>
    
  </refsection>
  
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
load(CWApath()+"demos/datafiles/RR_free.dat");
t=cumsum(RR);
outliers=sdFilter(RR,3);
[t_rm,RR_rm]=replaceRROutliers(t,RR,outliers,"remove");
[t_mean,RR_mean]=replaceRROutliers(t,RR,outliers,"mean",7);
[t_med,RR_med]=replaceRROutliers(t,RR,outliers,"median",7);
[t_splin,RR_splin]=replaceRROutliers(t,RR,outliers,"splin");
sel=1050:1080;
clf;plot(t_rm,RR_rm,t_mean,RR_mean,t_med,RR_med,t_splin,RR_splin,t,RR);
set(gca(),"zoom_box",[876 0.3 897 1.1])
legend(["remove","mean","median","splin","raw"],"in_lower_left");
    ]]></programlisting>
    <scilab:image><![CDATA[
load(CWApath()+"demos/datafiles/RR_free.dat");
t=cumsum(RR);
outliers=sdFilter(RR,3);
[t_rm,RR_rm]=replaceRROutliers(t,RR,outliers,"remove");
[t_mean,RR_mean]=replaceRROutliers(t,RR,outliers,"mean",7);
[t_med,RR_med]=replaceRROutliers(t,RR,outliers,"median",7);
[t_splin,RR_splin]=replaceRROutliers(t,RR,outliers,"splin");
sel=1050:1080;
clf;plot(t_rm,RR_rm,t_mean,RR_mean,t_med,RR_med,t_splin,RR_splin,t,RR);
set(gca(),"zoom_box",[876 0.3 897 1.1])
legend(["remove","mean","median","splin","raw"],"in_lower_left");
    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="percentFilter">percentFilter</link>
      </member>
      <member>
        <link linkend="medianFilter">medianFilter</link>
      </member>
       <member>
        <link linkend="threshFilter">threshFilter</link>
      </member>
      <member>
        <link linkend="sdFilter">sdFilter</link>
      </member>
 
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA, Scilab version</member>
      <member>John T. Ramshur, University of Memphis, Department of Biomedical Engineering,jramshur@gmail.com, Matlab version</member>
    </simplelist>
  </refsection>
 
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Functionadded</revdescription>
        </revision>
      </revhistory>
    </refsection>
  
</refentry>
