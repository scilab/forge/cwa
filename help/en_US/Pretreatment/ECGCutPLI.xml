<?xml version="1.0" encoding="UTF-8"?>
<!--
 This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2013 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xml:id="ECGCutPLI" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>ECGCutPLI</refname>

    <refpurpose>Removes power line interference in multi channel
    ECG</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>ecg_out = ECGCutPLI(ecg_in,fe,f [,df])</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>ecg_in</term>

        <listitem>
          <para>a real array, each column contains the signal samples of the
          corresponding channel.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>fe</term>

        <listitem>
          <para>a positive scalar, the sampling frequency in Hz.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>f</term>

        <listitem>
          <para>a positive scalar, the power line frequency in Hz.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>df</term>

        <listitem>
          <para>a positive scalar, defines the  band width (in Hz) to cut. The default value is 0.2Hz</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>ecg_out</term>

        <listitem>
          <para>a real array with identical shape as ecg_in, each column
          contains the filtered signal samples of the corresponding
          channel.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function removes power line interference in multi
    channel ECG. It zeroes the frequency components in the frequency
    bands [k*f-df k*f+df] for all k in [1 fe/(2*f)].</para>
  </refsection>

  <refsection>
    <title>More information</title>

    <important>
      <para>Sampling frequency fe must be a multiple of the PLI frequency
      f.</para>
    </important>

    <tip>
      <para>It is recommended to apply this function to detrended ECG (see
      <link linkend="ECGDetrend">ECGDetrend</link>).</para>
    </tip>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[
S=readEcgsFile(CWApath()+"demos/datafiles/ecg1.ecgs");
Sd=ECGDetrend(S);
Sf=ECGCutPLI(Sd,50,1);
clf;plot(Sd.sigs(200:3000));plot(Sf.sigs(200:3000),'r');
legend(["Sd","Sf"]);
ax1=newaxes();
plot(Sd.sigs(600:1500));plot(Sf.sigs(600:1500),'r');
ax1.axes_bounds=[0.18 0.125 0.35 0.45];ax1.axes_visible='off';
ax1.data_bounds(:,2)=[-0.02;0.02];
xstring(450,0.01,'Zoom')
    ]]></programlisting>

    <scilab:image><![CDATA[
S=readEcgsFile(CWApath()+"demos/datafiles/ecg1.ecgs");
Sd=ECGDetrend(S);
Sf=ECGCutPLI(Sd,50,1);
clf;plot(Sd.sigs(200:3000));plot(Sf.sigs(200:3000),'r');
legend(["Sd","Sf"]);
ax1=newaxes();
plot(Sd.sigs(600:1500));plot(Sf.sigs(600:1500),'r');
ax1.axes_bounds=[0.18 0.125 0.35 0.45];ax1.axes_visible='off';
ax1.data_bounds(:,2)=[-0.02;0.02];
xstring(450,0.01,'Zoom')
set(gcf(),"axes_size",[630,260])
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="ECGSubstractPLI">ECGSubstractPLI</link></member>
      <member><link linkend="ECGCutPLIBatch">ECGCutPLIBatch</link></member>
      <member><link linkend="ECGDetrend">ECGDetrend</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>



  <refsection>
    <title>History</title>

    <revhistory>
      <revision>
        <revnumber>0.0</revnumber>

        <revdescription>Function added</revdescription>
      </revision>
    </revhistory>
  </refsection>

  <refsection>
    <title>Used Functions</title>
    <para>This function is based on the <link
    linkend="scilab.help/fft" type="scilab">fft</link> and <link
    linkend="scilab.help/ifft" type="scilab">ifft</link>
    built-ins.</para>
    
  </refsection>
</refentry>
