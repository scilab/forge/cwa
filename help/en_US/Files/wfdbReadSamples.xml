<?xml version="1.0" encoding="UTF-8"?>
<!--
 This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2014 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xml:id="wfdbReadSamples" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 20-04-2012 $</pubdate>
  </info>

  <refnamediv>
    <refname>wfdbReadSamples</refname>

    <refpurpose>Reads the signals of a WFDB (MIT format) record.</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>[sig,fe,names]= wfdbReadSamples(record_name [,highfreq [,time_bounds [,sigsel]]])</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>record_name</term>

        <listitem>
          <para>A character string, the name of the WFDB database record
          (without extension)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>highfreq</term>

        <listitem>
          <para>A boolean. If true the signal files is read in high-resolution
          mode (default: standard mode). These modes are identical for
          ordinary records. For multi frequency records, the standard
          decimation of oversampled signals to the frame rate is suppressed in
          high-resolution mode (rather, all other signals are resampled at the
          highest sampling frequency).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>time_bounds</term>

        <listitem>
          <para>A two elements vector [begin_time, end_time] used to select a
          time range. If omitted or set to an empty array all the samples are
          returned. The arguments begin_time and end_time must be given in
          seconds.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>sigsel</term>

        <listitem>
          <para>A vector with integer values in [1, nsig] where nsig is the
          number of signals stored in the file or an empty array. This
          argument can be used to select the signals to be returned</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>sig</term>

        <listitem>
          <para>A floating point array. Each row contains a signal.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>fe</term>

        <listitem>
          <para>A floating point number: the sampling frequency.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>names</term>

        <listitem>
          <para>A character string column vector. The signal names</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function reads the signals of a WFDB record.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <para>Get and view all the signals stored in twa00 record.</para>

    <programlisting role="example"><![CDATA[
[sig,fe,n]=wfdbReadSamples(CWApath()+"demos/datafiles/twa00");
t=(0:size(sig,2)-1)/fe;
clf;plot(t',sig');
legend(n);
    ]]></programlisting>

    <scilab:image><![CDATA[
[sig,fe,n]=wfdbReadSamples(CWApath()+"demos/datafiles/twa00");
t=(0:size(sig,2)-1)/fe;
clf;plot(t',sig');
legend(n);
    ]]></scilab:image>

    <para>Get and view the first 10 seconds of the first channel.</para>

    <programlisting role="example"><![CDATA[
[sig,fe,n]=wfdbReadSamples(CWApath()+"demos/datafiles/twa00",%f,[0 10],1);
t=(0:size(sig,2)-1)/fe;
clf;plot(t',sig');
legend(n);
    ]]></programlisting>

    <scilab:image><![CDATA[
[sig,fe,n]=wfdbReadSamples(CWApath()+"demos/datafiles/twa00",%f,[0 10],1);
t=(0:size(sig,2)-1)/fe;
clf;plot(t',sig');
legend(n);
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link
      linkend="wfdbReadAnnotations">wfdbReadAnnotations</link></member>
      <member><link
      linkend="MIT2Ecgs">MIT2Ecgs</link></member>
    </simplelist>
  </refsection>

  <refsection>
     

    <title>Reference</title>

     <para>This function is based on the 

    <ulink url="http://www.physionet.org/physiotools/wfdb.shtml">WFDB
    library</ulink>.</para>


  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
</refentry>
