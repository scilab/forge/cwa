<?xml version="1.0" encoding="UTF-8"?>
<!--
 This file is part of the Cardiovascular Wawes Analysis toolbox
 Copyright (C) 2014 - INRIA - Serge Steer
 This file must be used under the terms of the CeCILL.
 This source file is licensed as described in the file COPYING, which
 you should have received as part of this distribution.  The terms
 are also available at
 http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="extractPartFromEcgsFile">
  <refnamediv>
    <refname>extractPartFromEcgsFile</refname>
    <refpurpose>Get part of a multi channels Scilab ECG file.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>ecg = extractPartFromEcgsFile(filein,from,N [,sigsel [,silent]])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>filein</term>
        <listitem>
          <para>
            A character string, the path of the Scilab ECG file.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>from</term>
        <listitem>
          <para>
            a scalar with integer value, the index of the first sample of the sequence.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>N</term>
        <listitem>
          <para>
            a scalar with integer value, the length of the sequence.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>sigsel</term>
        <listitem>
          <para>
            a vector of indexes, the selected channels. If omitted all channels are got.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>silent</term>
        <listitem>
          <para>
            a boolean, if true a progression bar is used.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>ecg</term>
        <listitem>
          <para>
            A <link linkend="sciecg">sciecg</link>data structure.
          </para>
        </listitem>
      </varlistentry>
      
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
        This function can be used to get all or part of a multi
        channels Scilab ECG file. It is particularly useful to work with  huge
        ECG that cannot be stored in memory.
    </para>
    
  </refsection>
  <refsection>
    <title>More information</title>
    <tip><para>The <link
    linkend="ecgsFileInfo">ecgsFileInfo</link> may be used to get
    the sampling frequency, number of channels and number of samples of the Scilab ECG
    file.</para></tip>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
[fs,NChannels,NSamples]=ecgsFileInfo(CWApath()+"demos/datafiles/LG.ecgs")
S=extractPartFromEcgsFile(CWApath()+"demos/datafiles/LG.ecgs", 10*fs,5*fs,[3 4]);
viewECG(S)
    ]]></programlisting>
    <scilab:image><![CDATA[
[fs,NChannels,NSamples]=ecgsFileInfo(CWApath()+"demos/datafiles/LG.ecgs")
S=extractPartFromEcgsFile(CWApath()+"demos/datafiles/LG.ecgs", 10*fs,5*fs,[3 4]);
viewECG(S)
    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member><link linkend="readEcgsFile">readEcgsFile</link></member>
      <member><link linkend="ecgsFileInfo">ecgsFileInfo</link></member>
      <member><link linkend="getValuesFromEcgsFile">getValuesFromEcgsFile</link></member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
  
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function  added</revdescription>
        </revision>
      </revhistory>
    </refsection>
</refentry>
