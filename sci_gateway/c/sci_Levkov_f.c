#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "malloc.h"
#include "sciprint.h"
extern int Levkov_f(int in,int out,int N,double *Threshold,int Nchannels, int Nsamples, double *temp);

int sci_Levkov_f(char *fname,void* pvApiCtx)
{
  int k=0;
  int fs=0, f=0;
  double t=0.0;
  double w=0.0;
  int in,out;
  int Nchannels = 0,Nsamples = 0, N = 0;
  double* Threshold= NULL;
  int mTh,nTh;
  double* work = NULL;
  SciErr sciErr;
  int *piAddressVar = NULL;

  CheckInputArgument(pvApiCtx, 7, 7);
  CheckOutputArgument(pvApiCtx, 1, 1);


  /*in*/
  k=1;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
  getScalarDouble(pvApiCtx, piAddressVar, &w);
  in=w;

  /*out*/
  k=2;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
  getScalarDouble(pvApiCtx, piAddressVar, &w);
  out=w;

  /*fs*/
  k=3;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
  }
  if (!isScalar(pvApiCtx, piAddressVar)) {
    Scierror(999, _("%s: Wrong size for input argument #%d.\n"), fname, k);
    return 0;
  }
  getScalarDouble(pvApiCtx, piAddressVar, &w);
  fs = (int)w;

  /*f*/
  k=4;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
  }
  if (!isScalar(pvApiCtx, piAddressVar)) {
    Scierror(999, _("%s: Wrong size for input argument #%d.\n"), fname, k);
    return 0;
  }
  getScalarDouble(pvApiCtx, piAddressVar, &w);
  f = (int)w;
 
  /*Threshold*/
  k=5;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
    Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
    return 0;
  }
  sciErr = getMatrixOfDouble(pvApiCtx, piAddressVar, &mTh, &nTh, &Threshold);
  if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return sciErr.iErr;
    }


  /*Nchannels*/
  k=6;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
  getScalarDouble(pvApiCtx, piAddressVar, &w);
  Nchannels=w;
  if (mTh*nTh!=Nchannels) {
    Scierror(999, _("%s: incompatible input arguments %d and %d\n"), fname, 5,6);
    return 0;
  }
 
  /*Nsamples*/
  k=7;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
  getScalarDouble(pvApiCtx, piAddressVar, &w);
  Nsamples=w;


  
  N=fs/f;
  /*work: temp(N*channels),src ((N+2)*NChannels),des(NChannels),A (N)*/
  work = (double*)malloc(sizeof(double)*((2*N+3)*Nchannels+N));

  Levkov_f(in,out,N,Threshold,Nchannels,Nsamples,work);
  free(work);
  AssignOutputVariable(pvApiCtx, 1) = 0;
  ReturnArguments(pvApiCtx);
  return 0;
}
