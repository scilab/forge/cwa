/*
* Scilab ( http://www.scilab.org/ ) - This file is part of wfdb toolbox
* Copyright (C) INRIA - 2013 - Serge Steer
*
* This file must be used under the terms of the CeCILL.
* This source file is licensed as described in the file COPYING, which
* you should have received as part of this distribution.  The terms
* are also available at
* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
*
*/
#include <time.h>
#include <stdio.h>
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "malloc.h"
#include "sciprint.h"
#include "wfdb.h"
#include "wfdblib.h"
#define map1
#define map2
#define ammap
#define mamap
#define annpos
#include "ecgmap.h"
#define Max(x,y)    (((x)>(y))?(x):(y))


void mput2 (FILE *fd, int swap, double *res, int n, char *type, int *ierr);

char *expandPathVariable(char *str);

int sci_MIT2Ecgs(char *fname, void* pvApiCtx)
{
 
  SciErr sciErr;
  int *piAddressVar = NULL;
  char *record = NULL;
  const char *outpath = NULL;
  FILE * fdout = NULL;
  double dres = 0.0;
  char *file_type="ECGScilab";
  short s=0;
  float fe;

  int nsig = 0;
  int iCols = 0;
  double *Signals = NULL;
  double fsampling = 0.0;
  char **signames = NULL;

  WFDB_Frequency freq;
  WFDB_Sample *v = NULL;
  WFDB_Siginfo *si= NULL;

  int highres = 0;
  int from = 1;
  int to = 0;
  int nin= nbInputArgument(pvApiCtx);
  int nout= nbOutputArgument(pvApiCtx);
  double* sigsel = NULL;
  int *sel = NULL;
  double* timebounds = NULL;
  int nsel = 0;
  char *expandedPath=NULL;
 

  int k=0, i=0, j=0, m=0, n=0 ;
  CheckInputArgument(pvApiCtx, 1, 5);
  CheckOutputArgument(pvApiCtx, 0, 1);
  /*Record name */
  sciErr = getVarAddressFromPosition(pvApiCtx,1 , &piAddressVar);
  if(getAllocatedSingleString(pvApiCtx, piAddressVar, &record)) goto ERR;
  expandedPath = expandPathVariable(record);
  freeAllocatedSingleString(record);
  if (expandedPath==NULL) {
    Scierror(999, _("%s: No more memory.\n"), fname);
    return 0;
  }
  /*output file path */
  sciErr = getVarAddressFromPosition(pvApiCtx,2 , &piAddressVar);
  if(getAllocatedSingleString(pvApiCtx, piAddressVar, &record)) goto ERR;
  outpath=expandPathVariable(record);
  freeAllocatedSingleString(record);
  if (outpath==NULL) {
    Scierror(999, _("%s: No more memory.\n"), fname);
    return 0;
  }
 
  if (nin>=3) { /*highres*/
    sciErr = getVarAddressFromPosition(pvApiCtx,3 , &piAddressVar);
    if(getScalarBoolean(pvApiCtx, piAddressVar,&highres) ) goto ERR;
    if (nin>=4) { /*time selection*/
      sciErr = getVarAddressFromPosition(pvApiCtx,4 , &piAddressVar);
      sciErr = getMatrixOfDouble(pvApiCtx, piAddressVar, &m, &n, &timebounds);
      if(sciErr.iErr)
        {
          printError(&sciErr, 0);
          goto ERR;
        }
      if (m*n != 2 && m*n != 0) {
        Scierror(999,_("%s: Wrong size for input argument #%d: %d or %d expected.\n"),"MIT2Ecgs",3,0,2);
        goto ERR;
      }
      if (nin==5) { /*signal selection*/
        sciErr = getVarAddressFromPosition(pvApiCtx,5 , &piAddressVar);
        sciErr = getMatrixOfDouble(pvApiCtx, piAddressVar, &m, &n, &sigsel);
        if(sciErr.iErr)
          {
            printError(&sciErr, 0);
            goto ERR;
          }
        nsel=m*n;
      }
      else nsel=0;
    }
  }
  wfdbquiet();
  if ((nsig = isigopen(expandedPath, NULL, 0)) <= 0){
    Scierror(999,_("%s: wfdb  error: %s.\n"),"MIT2Ecgs",wfdberror());
    goto ERR;
  }


  /* Create the signals selection array */
  if ((sel = malloc(Max(nsel,nsig) * sizeof(int))) == NULL){
    Scierror(999,_("%s: Cannot allocate more memory.\n"),"MIT2Ecgs");
    goto ERR;
  }
  if (nsel) {
    for (k=0; k<nsel; k++) {
      sel[k] = (int)sigsel[k]-1;
      if ((sel[k]<0) || (sel[k]>=nsig)) {
        Scierror(999,_("%s: Wrong value for input argument #%d: Must be in the interval [%d, %d].\n"),"MIT2Ecgs",4,1,nsig);
        goto ERR;
      }
    }
  }
  else {
    for (k=0; k<nsig; k++)  sel[k] = k;
    nsel = nsig;
  }


  if ((v = malloc(nsig * sizeof(WFDB_Sample))) == NULL ||
      (si = malloc(nsig * sizeof(WFDB_Siginfo))) == NULL) {
    Scierror(999,_("%s: Cannot allocate more memory.\n"),"MIT2Ecgs");
    goto ERR;
  }
  if ((nsig = isigopen(expandedPath, si, nsig)) <= 0) {
     Scierror(999,_("%s: wfdb  error: %s.\n"),"MIT2Ecgs",wfdberror());
    goto ERR;
  }
  if (highres)
    setgvmode(WFDB_HIGHRES);

  freq = sampfreq(NULL);

  /* count the number of samples */
  while ( getvec(v) >= 0)   iCols++;
  if (timebounds != NULL) {
    from=(int)(timebounds[0]*freq)+1;
    to=(int)(timebounds[1]*freq)+1;
    if (to == 0||to > iCols) to=iCols;
    if (from > to) {
      Scierror(999,_("%s: Wrong values for input argument #%d: Elements must be in increasing ""order.\n"),"MIT2Ecgs",3);
      goto ERR;
    }
  }
  else {
    from=1;
    to=iCols;
  }

  iCols = to + 1 - from;

  if ((isigopen(expandedPath, si, nsig)) <= 0) {
    Scierror(999,_("%s: wfdb returned an error, see above.\n"),"rdsamp");
    goto ERR;
  }

  for (i = 0; i < nsig; i++)
    if (si[i].gain == 0.0) si[i].gain = WFDB_DEFGAIN;

  if (highres)
    setgvmode(WFDB_HIGHRES);
  fdout=fopen(outpath, "wb");
 
  if (fdout == NULL) {
    Scierror(999,_("%s: Destination file cannot be opened.\n"),"MIT2Ecgs");
    goto ERR1;
  }

  if (fwrite(file_type,sizeof(char),9,fdout) !=9) goto ERR;
  s=0;
  for (i=0;i<6;i++) {
    if (fwrite(&s,sizeof(short),1,fdout) !=1) goto ERR;
  }

  if (fwrite(&nsel,sizeof(int),1,fdout) !=1) goto ERR;
  if (fwrite(&iCols,sizeof(int),1,fdout) !=1) goto ERR;
  fe=freq;
  if (fwrite(&fe,sizeof(float),1,fdout) !=1) goto ERR;

  j=0;
  while ( getvec(v) >= 0) {
    j++;
    if (j>to) break;
    if (j>=from) {
      for (i = 0; i < nsel; i++) {
        dres=((double)v[sel[i]] - si[sel[i]].baseline)/si[sel[i]].gain;
	if (fwrite(&dres,sizeof(double),1,fdout) !=1) goto ERR;
       
      }
    }
  }
  fclose(fdout);

  AssignOutputVariable(pvApiCtx, 1) = 0;
 

  FREE(expandedPath);
  expandedPath = NULL;
  FREE(outpath);outpath=NULL;

  free(signames);
  free(si);
  free(v);
  free(sel);
  wfdb_sigclose();
  ReturnArguments(pvApiCtx);
  return 0;
 ERR:
  fclose(fdout);
 ERR1:
  FREE(expandedPath);
  expandedPath = NULL;
  free(signames);
  free(si);
  free(v);
  free(sel);
   wfdb_sigclose();
  return 0;
}
