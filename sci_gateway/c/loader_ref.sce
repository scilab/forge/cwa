function CWA_gateway_loader()
  libsciCWA_path = get_absolute_file_path('loader.sce');
  list_functions = [ 'wfdbReadAnnotations';
                     'wfdbReadSamples';
                     'MIT2Ecgs';
                     'Levkov';
                     'Levkov_f';
                     'Lomb';
                     'SampEn';
                   ];
  // ulink previous function with same name
  [bOK, ilib] = c_link('libsciCWA');
  if bOK then ulink(ilib);end

  link(libsciCWA_path + '../../src/c/libCWA' + getdynlibext());
  if getos()=="Windows" then
    link(SCI+"\bin\fileio"+ getdynlibext());
      addinter(libsciCWA_path+'sciCWA'+getdynlibext(),'sciCWA',list_functions);
  else
    addinter(libsciCWA_path+'libsciCWA'+getdynlibext(),'libsciCWA',list_functions);
  end
endfunction
CWA_gateway_loader()
clear CWA_gateway_loader;

